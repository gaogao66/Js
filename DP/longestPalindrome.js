// 最长回文子串
var longestPalindrome = function (s) {
    var arr = s.split('');
    let index = 0, max = 1;
    for (let i = 0; i < arr.length; i++) {
        let index1 = i, max1 = 1;
        for (let j = i + 1; j <= arr.length; j++) {
            let str = arr.slice(i, j);
            let re = [...str].reverse().join('');
            if (str.join('') === re) {
                j > max1 ? max1 = j : null;
            }
        }
        if (max1 - index1 > max - index) {
            index = index1;
            max = max1;
        }
    }

    console.log(index, max, arr.slice(index, max).join(''))
    return arr.slice(index, max).join('');
};

longestPalindrome("xabay")

// 中心法
var longestPalindrome = function (s) {
    if (s.length === 1) {
        return s;
    } else if (s.length === 2) {
        return s[0] == s[1] ? s : s[0];
    }
    let start = 0, end = 0;
    for (let i = 0; i < s.length; i++) {
        let len1 = getMaxLen(s, i, i);// 中心为元素
        let len2 = getMaxLen(s, i, i + 1);// 中心不是元素
        let len = Math.max(len1, len2);
        if (end - start < len) {
            let d = Math.floor(len / 2)
            if (len1 > len2) {
                start = i - d;
            } else {
                start = i - (d - 1);
            }
            end = i + d;
        }
    }
    console.log(start, end + 1);
    return s.slice(start, end + 1);

    function getMaxLen(s, left, right) {
        while (left >= 0 && right < s.length && s[left] == s[right]) {
            left--;
            right++;
        }
        return right - left - 1;
    }
};

// 待做——动态规划
function longestPalindrome(s) {
    let len = s.length;
    if (len < 2) {
        return s;
    }

    let maxLen = 1;
    let begin = 0;
    // dp[i][j] 表示 s[i..j] 是否是回文串
    let dp = new boolean[len][len];
    // 初始化：所有长度为 1 的子串都是回文串
    for (let i = 0; i < len; i++) {
        dp[i][i] = true;
    }

    // char[] charArray = s.toCharArray();
    let charArray = s.split('');
    // 递推开始
    // 先枚举子串长度
    for (let L = 2; L <= len; L++) {
        // 枚举左边界，左边界的上限设置可以宽松一些
        for (let i = 0; i < len; i++) {
            // 由 L 和 i 可以确定右边界，即 j - i + 1 = L 得
            let j = L + i - 1;
            // 如果右边界越界，就可以退出当前循环
            if (j >= len) {
                break;
            }

            if (charArray[i] != charArray[j]) {
                dp[i][j] = false;
            } else {
                if (j - i < 3) {
                    dp[i][j] = true;
                } else {
                    dp[i][j] = dp[i + 1][j - 1];
                }
            }

            // 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
            if (dp[i][j] && j - i + 1 > maxLen) {
                maxLen = j - i + 1;
                begin = i;
            }
        }
    }
    return s.substring(begin, begin + maxLen);
}

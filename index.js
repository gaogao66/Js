function delay(time) {
    return new Promise(reslove => {
        setTimeout(reslove, time);
    });
}

function methodDelay(promise, time) {
    // promise即checkVote()
    let timeout = this.delay(time).then(() => {
      throw new TimeoutExceptions("time out!");
    });
    return Promise.race(promise, timeout);
  }
function checkVote() {
    let delay = new Promise(reslove => {
      setTimeout(reslove, 5000);
    }).then(() => {
      throw new TimeoutExceptions("time out!");
    });
    let checkVoteFunction = () => {};
    return Promise.race(checkVoteFunction, delay);
  }
  // 以下是原始的封装代码示例
async function checkVoteMuti(serverList, voteId) {
    // const resultList = [];
    let result = {};
    for (var i = 0; i <= serverList.length; i++) {
      try {
        let state = await checkVote(serverList[i], voteId).toString();
        result[state] ? result[state]++ : (result[state] = 1);
      } catch (e) {
        throw new error();
        // resultList.push(0);
      }
    }
    const halfMore = Math.ceil(serverList.length / 2);
    if (result["1"] >= halfMore) return 1;
    if (result["-1"] >= halfMore) return -1;
    // if (resultList.filter(p => p === 1).length >= halfMore) return 1;
    // if (resultList.filter(p => p === -1).length >= halfMore) return -1;
    return 0;
  }
  // MergeTopOf(1, [1,3,4],[2,7],[5]) 返回 [1] 堆排，多路归并
const Heap = require("heap");

function MergeTopOf(n, ...rest) {
    let heap = new Heap(); // {value: '', source: '', current: ''} (a,b) => {return b-a}
    let arrayList = rest;
    let result = [];
    // 初始化最小堆
    arrayList.forEach((item,index) => {
        heap.push({value: item[0],source: index, current: 0});
    });
    // 取堆顶元素并填充数据，来源同一
    while(result.length < n) {
        let temp = new Heap();
        let smallest = heap.peek();
        console.log("smallest", smallest)
        result.push(smallest.value);
        let obj = {
            value: arrayList[smallest.source][++smallest.current],
            source: smallest.source,
            current: smallest.current
        }
        heap.replace(obj);
        // heap.heapify(); 无法全部调整小顶堆，只能调整修改了的值
        heap.updateItem(obj);
        console.log(heap)
    }
    return result;
}
// MergeTopOf(3, [1,3,4],[2,7],[5]);
console.log(MergeTopOf(3, [1,3,4],[2,7],[5]));




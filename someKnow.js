// Map——Map对象保存键值对，任何值都可以作为键或者值
// 1. Object的键只能是字符串或Symbols, Map的键可以是任意值
// 2. Map中的键值是有序的（FIFO）
// 3. Map的键值对个数可以从size属性获取、
// 4. Object都有自己的原型，原型链键名有可能和对象上设置的键名产生冲突
// 操作
// 1. Map构造函数可以将一个二维键值对数组转换成一个Map对象
// 2. Array.from可以将一个Map对象转换成一个二维键值对数组
// [["key1", "value1"], ["key2", "value2"]]
// Map复制，new Map()
// Map合并，new Map([...map1, ...map2]);重复键值，后面覆盖前面
// keys()，values()获取键、值Map对象

// es6数组操作
// 1. arr.find() 返回符合条件的数组成员，没找到返回undefined
// 2. arr.findIndex() 找到的是位置，找不到-1
// 3. arr.fill(填充的东西，开始位置，结束位置)
// 4. arr.includes()数组是否包含某个元素 true|false
// 5. arr.flat() 扁平化，返回新数组，不影响原数组；flat参数默认拉平1层，整数代表拉平层数，Infinity参数表示任意层，flat会跳过空位
// 6. arr.flatMap()对原数组的每一个成员执行一个函数，相当于执行map(),然后对返回值数组执行flat()方法，返回新数组，只能展开1层


// es6 super类似es5中call继承
//   class静态属性和静态方法：不会被实例继承，只能直接通过类来调用，可被子类继承， 可从super对象上调用。
// 内存泄漏
// 未声明的变量；console.log中的变量，需要在开发工具查看；闭包；定时器；dom泄露（dom节点的引用）
// dom泄露
// var select = document.querySelector;
// var treeRef = select('#tree');

// var leafRef = select('#leaf');   //在COM树中leafRef是treeFre的一个子结点

// select('body').removeChild(treeRef);//#tree不能被回收入，因为treeRef还在
//  　　解决方法:

// treeRef = null;//tree还不能被回收，因为叶子结果leafRef还在
// leafRef = null;//现在#tree可以被释放了
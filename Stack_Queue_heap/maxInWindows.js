// 滑动窗口的最大值
// 时间复杂度O(n),空间复杂度O(n)

// 方法1: 暴力解题的优化
// 思路：每次记录当前的最大值是不是窗口的第一个值，是则说明下一个窗口需要重新计算最大值，否则只需比较最大值与新加入的值的大小。
// 时间复杂度：最坏 O(n * k), 最好O(n),其中n为数组大小，k为窗口大小
// 空间复杂度：O(1)，存结果必须要开的数组不算入额外空间
function maxInWindows(num, size) {
    if (size > num.length || size === 0) return [];
    const result = [];
    let oneIsMax = true;
    for (let i = 0; i < num.length - size + 1; i++) {
        let max;
        if (oneIsMax) {
            let count = 0;
            max = -Infinity;
            while (count < size) {
                max = max >= num[i + count] ? max : num[i + count];
                count++;
            }
            result.push(max);
        } else {
            const preMax = result[result.length - 1], nextNum = num[i + size - 1];
            max = preMax >= nextNum ? preMax : nextNum;
            result.push(max);
        }
        oneIsMax = max === num[i] ? true : false;
    }
    return result;
}
// 方法2: 双向队列：可以从头和尾进行插入和删除的数据结构。
// 时间复杂度O(n),空间复杂度O(n)
// 思路：
// 我们都知道，若是一个数字A进入窗口后，若是比窗口内其他数字都大，那么这个数字之前的数字都没用了，因为它们必定会比A早离开窗口，在A离开之前都争不过A，所以A在进入时依次从尾部排除掉之前的小值再进入，而每次窗口移动要弹出窗口最前面值，因此队首也需要弹出，所以我们选择双向队列。
// 具体做法：
// step 1：维护一个双向队列，用来存储数列的下标。
// step 2：首先检查窗口大小与数组大小。
// step 3：先遍历第一个窗口，如果即将进入队列的下标的值大于队列后方的值，依次将小于的值拿出来去掉，再加入，保证队列是递增序。
// step 4：遍历后续窗口，每次取出队首就是最大值，如果某个下标已经过了窗口，则从队列前方将其弹出。
// step 5：对于之后的窗口，重复step 3，直到数组结束。
function maxInWindows(num, size) {
    const res = [];
    if (size > num.length || size === 0) return res;
    // 双向队列
    const dq = [];
    // 先遍历一个窗口
    for (let i = 0; i < size; i++) {
        // 去掉比自己先进队列的小于自己的值
        while (dq.length > 0 && num[dq[dq.length - 1]] < num[i]) {
            dq.pop();
        }
        dq.push(i);
    }
    // 遍历后续数组元素
    for (let i = size; i < num.length; i++) {
        // 取窗口内的最大值
        res.push(num[dq[0]]);
        while (dq.length > 0 && dq[0] < (i - size + 1)) {
            // 弹出窗口移走后的值
            dq.shift();
        }
        // 加入新的值前，去掉比自己先进队列的小于自己的值
        while (dq.length > 0 && num[dq[dq.length - 1]] < num[i]) {
            dq.pop();
        }
        dq.push(i);
    }
    res.push(num[dq[0]]);
    return res;
}
module.exports = {
    maxInWindows: maxInWindows
};
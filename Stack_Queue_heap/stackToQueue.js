// 用两个栈实现队列

// 解题思路：
// 借助栈的先进后出规则模拟实现队列的先进先出
// 1、当插入时，直接插入 stack1
// 2、当弹出时，当 stack2 不为空，弹出 stack2 栈顶元素，如果 stack2 为空，将 stack1 中的全部数逐个出栈入栈 stack2，再弹出 stack2 栈顶元素

// 时间复杂度：对于插入和删除操作，时间复杂度均为 O(1)。插入不多说，对于删除操作，虽然看起来是 O(n)O(n) 的时间复杂度，但是仔细考虑下每个元素只会「至多被插入和弹出 stack2 一次」，因此均摊下来每个元素被删除的时间复杂度仍为 O(1)。
// 空间复杂度O(N)：辅助栈的空间，最差的情况下两个栈共存储N个元素

const stack1 = [], stack2 = [];
function push(node) {
    // write code here
    stack1.push(node);
}
function pop() {
    // write code here
    if (stack2.length <= 0) {
        while (stack1.length) {
            stack2.push(stack1.pop())
        }
    }
    return stack2.pop();
}
module.exports = {
    push: push,
    pop: pop
};
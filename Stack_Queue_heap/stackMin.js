// 含min函数的栈
// 时间复杂度：O(1)，每个函数访问都是直接访问，无循环
// 空间复杂度：O(n)，s1为必要空间，s2为辅助空间

// 具体做法：
// step 1：使用一个栈记录进入栈的元素，正常进行push、pop、top操作。
// step 2：使用另一个栈记录每次push进入的最小值。
// step 3：pop出栈时，都要使最小值栈出栈后，取栈顶元素为最小值


const stack = [], stack2_Min = [];
let minNum = Infinity;

function push(node) {
    // write code here
    stack.push(node);
    minNum = minNum > node ? node : minNum;
    stack2_Min.push(minNum);
}
function pop() {
    if (stack.length) {
        // 每次出栈都要使最小值栈出栈后，取栈顶元素为最小值
        stack2_Min.pop();
        minNum = stack2_Min[stack2_Min.length - 1];
        return stack.pop();
    }
}
function top() {
    if (stack.length) {
        return stack[stack.length - 1];
    }
}
function min() {
    return minNum;
}
module.exports = {
    push: push,
    pop: pop,
    top: top,
    min: min
};
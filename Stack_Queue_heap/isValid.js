// 有效括号序列
// 时间复杂度O(N),空间复杂度O(N)
// 核心思想：
// 每次遇到左括号的时候，将字符入栈stk；而每次遇到右括号的时候则让对应的匹配字符出栈。

function isValid(s) {
    const left = {
        "(": 1,
        "{": 2,
        "[": 3,
    },
        right = {
            ")": 1,
            "}": 2,
            "]": 3,
        };
    const stack = [];
    for (let i = 0; i < s.length; i++) {
        if (left[s[i]]) {
            stack.push(s[i]);
        } else {
            let temp = stack[stack.length - 1];
            if (right[s[i]] === left[temp]) {
                stack.pop();
            } else {
                return false;
            }
        }
    }
    return stack.length === 0 ? true : false;
}
module.exports = {
    isValid: isValid,
};
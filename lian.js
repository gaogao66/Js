function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}
// 构建链表
function makeNum(arr) {
    let index = 1;
    let result = new ListNode(arr[0]);
    while(index < arr.length) {
        // 指针指向链表节点时，指针的值存的是下一个实例的地址
        let temp = result;
        // 每一次从result找到最后一个链表元素
        while(temp.next) {
            temp = temp.next;
        }
        temp.next = new ListNode(arr[index]);
        index++;
    }
    return result;
}
const l1 = makeNum([9,9,9,9,9,9,9]);
const l2 = makeNum([9,9,9,9]);
// console.log(l1, l2) [8,9,9,9,0,0,0,1]
// 大数相加
var addTwoNumbers = function(l1, l2) {
    let over = 0;
    let result;
    while(l1 && l2) {
        let temp = result
        let add = over + l1.val + l2.val;
        over = Math.floor(add / 10);
        if (!temp) {
            result = new ListNode(add % 10);
        } else {
            temp = findNext(result);
            temp.next = new ListNode(add % 10)
        }
        l1 = l1.next;
        l2 = l2.next;
    }
    let rest = l1 || l2;
    while (rest) {
        let temp = findNext(result);
        let add = rest.val;
        if (over) {
            add += over;
            over = Math.floor(add / 10);
        }
        temp.next = new ListNode(add % 10);
        rest = rest.next;
    }
    // over有值
    over ? findNext(result).next = new ListNode(over) : null;
    console.log(result);
    return result;
};

function findNext(result) {
    let temp = result;
    while(temp.next) {
        temp = temp.next;
    }
    return temp;
}
addTwoNumbers(l1, l2);

// 链表反转
var reverseList = function(head) {
    let result = new ListNode(), p = head;
    while(p) {
        let temp = result.next;
        result.next = new ListNode(p.val, temp);
        p = p.next;
    }
    return result.next;
};

var reverseList = function(head) {
    let cur = null, pre = head;
    while(pre) {
        let t = pre.next;
        pre.next = cur;
        cur = pre;
        pre = t;
    }
    return cur;
};
// 指定区间反转
function reverseBetween( head ,  m ,  n ) {
    // write code here
    let pre, cur = null, len = n - m + 1, temp = head, front = head, num = m;
    while(temp && --m) {
        m !== 1 ? front = front.next : null;
        temp = temp.next;
    }
    pre = temp; 
    while(len && pre){
        let t = pre.next;
        pre.next = cur;
        cur = pre;
        pre = t;
        len--;
    }

    pre ? temp.next = pre : null;
    num !== 1 ? front.next = cur : head = cur;
    return head;
}
// 链表中的节点每k个一组翻转
function reverseKGroup( head ,  k ) {
    let pre = head, front = head,odd = 1;
    while(pre) {
        let c = k, temp = pre;
        while(--c && temp) {
            temp = temp.next;
        }
        if (temp) {
            c = k;
            let curTemp = null, newF = pre;
            while(c--) {
                let t = pre.next;
                pre.next = curTemp;
                curTemp = pre;
                pre = t;
            }
            odd === 1 ? (odd = 2, head = curTemp) : (front.next = curTemp, front = newF);
        } else {
            odd === 1 ? odd = 2 : (front.next = pre);
            return head;
        }
    }
    return head;
}
// 链表合并
var mergeKLists = function(lists) {
    if (lists.length == 0) return null;
    let head = lists[0];
    for(let i = 1; i < lists.length; i++) {
        if(lists[i]) {
            head = mergeList(lists[i], head)
        }
    }
    return head;
};
// 空间复杂度o(1)
function Merge(pHead1, pHead2)
{
    let result = new ListNode(0), temp = result;
    while(pHead1 && pHead2) {
        if (pHead1.val <= pHead2.val) {
            temp.next = pHead1;
            pHead1 = pHead1.next;
        } else {
            temp.next = pHead2;
            pHead2 = pHead2.next;
        }
        temp = temp.next;
    }
    temp.next = pHead1 || pHead2;
    return result.next;
}

// 判断链表中是否有环
// 1. 刚开始用对象存元素，觉得出现重复元素就应该是环，但是到一个用例中，有重复元素形成环，但是可以出去的， 答案是false。也就是说可以出去的环并不是真正意义上的环。
// 2. 所以用快慢两个指针，快的指针一次走两步，要是成环终会相遇，不相遇则没成环。
function hasCycle( head ) {
    let fast = head, slow = head;
    while(fast && fast.next) {
        slow = slow.next;
        fast = fast.next.next;
        if (slow === fast) return true;
    }
    return false;
}
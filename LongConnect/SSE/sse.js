const http = require("http");

http.createServer(function(req, res) {
    console.log(req.url)
    if (req.url === "/sse") {
        res.writeHead(200, {
            "Content-Type": "text/event-stream",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Access-Control-Allow-Origin": "*"
        });
        res.write("retry: 1000\n");
        res.write("event: connectTime\n");
        res.write("data:" + new Date() + "\n\n");
        res.write("data:" + new Date() + "\n\n");

        interval = setInterval(function() {
            res.write("data:" + new Date() + "\n\n");
        }, 1000);

        req.addListener("close", function() {
            clearInterval(interval)
        }, false)
    }
}).listen(3000, '127.0.0.1');
const http = require('http');
const webSocketServer = require('websocket').server;

const server = http.createServer(function(req, res) {
    // not important writeing websocket
}).listen(3000, () => {
    console.log("listen on 127.0.0.1:3000 ...")
})

ws = new webSocketServer({
    httpServer: server
});

ws.on('request', function(request) {
    const connection = request.accept('', request.origin);
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        } else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    })
})

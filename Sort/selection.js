function selection(...arr) {
    let len = arr.length;
    for(let i = 0; i < len; i++) {
        let minIndex = i;
        // 寻找未排序队列中最小的数下标
        for(let j = i+1; j < len; j++) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
        // 交换位置即向有序队列中添加最小值
        if(minIndex !== i) {
            arr[minIndex] = arr[minIndex] - arr[i];
            arr[i] = arr[minIndex] + arr[i];
            arr[minIndex] = arr[i] - arr[minIndex];
        }
        // 相同位置的数交换结果0，新加判断
    }
    console.log(arr);
    return arr;
}
selection(1,3,4,5,2,8,7,3,4);


function selection(...arr) {
    for(let i = 0; i < arr.length; i++) {
        let index = i;
        for(let j = i; j < arr.length; j++) {
            if (arr[j] > arr[index]) {
                index = j;
            }
        }
        arr.unshift(arr.splice(index, 1)[0])
    }
    console.log(arr);
    return arr;
} 

// 选择排序（Selection Sort）
// 选择排序(Selection-sort)是一种简单直观的排序算法。
// 它的工作原理：首先在未排序序列中找到最小（大）元素，存放到排序序列的起始位置，然后，
// 再从剩余未排序元素中继续寻找最小（大）元素
// 然后放到已排序序列的末尾。以此类推，直到所有元素均排序完毕。 

// 2.1 算法描述
// n个记录的直接选择排序可经过n-1趟直接选择排序得到有序结果。具体算法描述如下：

// 初始状态：无序区为R[1..n]，有序区为空；
// 第i趟排序(i=1,2,3…n-1)开始时，当前有序区和无序区分别为R[1..i-1]和R(i..n）。该趟排序从当前无序区中-选出关键字最小的记录 R[k]，将它与无序区的第1个记录R交换，使R[1..i]和R[i+1..n)分别变为记录个数增加1个的新有序区和记录个数减少1个的新无序区；
// n-1趟结束，数组有序化了。
// 表现最稳定的排序算法之一，因为无论什么数据进去都是O(n2)的时间复杂度，所以用到它的时候，数据规模越小越好。唯一的好处可能就是不占用额外的内存空间了吧。
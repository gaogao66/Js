// 相邻分组全部比较完
function shell(...arr) {
    let len = arr.length;
    // 分组大小
    for(let gap = Math.floor(len/2); gap > 0; gap = Math.floor(gap/2)) {
        // 分组组内元素
        for(let i = gap; i < len ; i++) {
            let j = i;
            let cur = arr[i];
            while(j - gap >= 0 && cur < arr[j - gap]) {
                arr[j] = arr[j-gap];
                j -= gap;
            }
            arr[j] = cur;
        }
    }
    console.log(arr);
    return arr;
}
shell(1,3,4,5,2,8,7,3,4);
// 和图一致
function shell(...arr) {
    for(let gap =  Math.floor(arr.length / 2); gap > 0; gap = Math.floor(gap / 2)) {
        for(let i = 0; i < gap; i++) {
            for (let j = i; j < arr.length && arr[j + gap] ; j += gap){
                if (arr[j] > arr[j + gap]) {
                    let temp = arr[j];
                    arr[j] = arr[j + gap];
                    arr[j + gap] = temp;
                }
            }
        }
    }
    console.log(arr);
    return arr;
}
// 希尔排序（Shell Sort）
// 1959年Shell发明，第一个突破O(n2)的排序算法，是简单插入排序的改进版。
// 它与插入排序的不同之处在于，它会优先比较距离较远的元素。希尔排序又叫缩小增量排序。
// 希尔排序的核心在于间隔序列的设定。既可以提前设定好间隔序列，也可以动态的定义间隔序列。

// 4.1 算法描述
// 先将整个待排序的记录序列分割成为若干子序列分别进行直接插入排序，具体算法描述：

// 选择一个增量序列t1，t2，…，tk，其中ti>tj，tk=1；
// 按增量序列个数k，对序列进行k 趟排序；
// 每趟排序，根据对应的增量ti，将待排序列分割成若干长度为m 的子序列，分别对各子表进行直接插入排序。
// 仅增量因子为1 时，整个序列作为一个表来处理，表长度即为整个序列的长度。
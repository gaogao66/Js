function mergeSort(arr) {
    let len = arr.length;
    if (len < 2) return arr;
    let middle = Math.floor(len / 2);
    return merge(mergeSort(arr.slice(0, middle)),mergeSort(arr.slice(middle)));
}
function merge(left, right) {
    let result = [];
    while(left.length && right.length) {
        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
    while(left.length) result.push(left.shift());
    while(right.length) result.push(right.shift());
    console.log(result);
    return result;
}
mergeSort([1,3,4,5,2,8,7,3,4]);
// 归并排序是建立在归并操作上的一种有效的排序算法。
// 该算法是采用分治法（Divide and Conquer）的一个非常典型的应用。
// 将已有序的子序列合并，得到完全有序的序列；即先使每个子序列有序，再使子序列段间有序。
// 若将两个有序表合并成一个有序表，称为2-路归并。 
// 归并排序是一种稳定的排序方法。和选择排序一样，归并排序的性能不受输入数据的影响，
// 但表现比选择排序好的多，因为始终都是O(nlogn）的时间复杂度。代价是需要额外的内存空间。
// 5.1 算法描述
// 把长度为n的输入序列分成两个长度为n/2的子序列；
// 对这两个子序列分别采用归并排序；
// 将两个排序好的子序列合并成一个最终的排序序列。
// 快速排序的基本思想：通过一趟排序将待排记录分隔成独立的两部分，其中一部分记录的关键字均比另一部分的关键字小，
// 则可分别对这两部分记录继续进行排序，以达到整个序列有序。

// 6.1 算法描述
// 快速排序使用分治法来把一个串（list）分为两个子串（sub-lists）。具体算法描述如下：

// 从数列中挑出一个元素，称为 “基准”（pivot）；
// 重新排序数列，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面（相同的数可以到任一边）。
// 在这个分区退出之后，该基准就处于数列的中间位置。这个称为分区（partition）操作；
// 递归地（recursive）把小于基准值元素的子数列和大于基准值元素的子数列排序。

// 递归

// 想到哪写到哪，最简单的实现
function quick(arr) {
    let len = arr.length;
    if (len <= 1) { return arr; }
    let left = [];
    let right = [];
    let pivot = arr.splice(0, 1)[0]; // 基点从原数组中删除
    for (let i = 0; i < len - 1; i++) {
        pivot >= arr[i] ? left.push(arr[i]) : right.push(arr[i]);
    }
    arr = [].concat(quick(left), [pivot], quick(right));
    console.log(arr);
    return arr;
}
// quick([1,3,4,5,2,8,7,3,4]);
// 主要通过计算基值的位置下标，left,right,pivot都只是下标
function quickSort(arr, left, right) {
    let len = arr.length;
    left = typeof left != "number" ? 0 : left;
    right = typeof right != "number" ? len : right;
    let pivotIndex = 0;
    if (left < right) {
        pivotIndex = getPivotIndex(arr, left, right);
        quickSort(arr, left, pivotIndex);
        quickSort(arr, pivotIndex + 1, right);
    }
    console.log(arr);
    return arr;
}
function getPivotIndex(arr, left, right) {
    let pivot = left, index = pivot + 1;
    for (let i = index; i < right; i++) {
        if (arr[i] < arr[pivot]) {
            change(arr, i, index); // 4,2,3,5,6,7,1的 1的情况
            index++;
        }
    }
    change(arr, pivot, index - 1);
    return index - 1;// pivot所在地
}
// 交换两个值
function change(arr, i, j) {
    // arr[i] = arr[i] - arr[j];
    // arr[j] = arr[i] + arr[j];
    // arr[i] = arr[j] - arr[i];
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}
quickSort([1, 3, 4, 5, 2, 8, 7, 3, 4]);

// 非递归

// 当要排序的数很多时，可能导致栈溢出，因此需要非递归的快排算法。
// 这里采用栈 + 循环来模拟递归调用过程，时间效率上和调用递归并无很大差别。
// 本质上和调用递归的过程一样
function quickSort1(arr, begin, end) {
    const stock = [];
    if (begin < end) {
        stock.push(begin);
        stock.push(end);
    }
    while (stock.length !== 0) {
        let right = stock.pop();
        let left = stock.pop();
        let mid = partSort2(arr, left, right);
        if (left < mid - 1) {
            stock.push(left);
            stock.push(mid - 1);
        }
        if (right > mid + 1) {
            stock.push(mid + 1);
            stock.push(right);
        }
    }
    return arr;
}
//挖坑法
function partSort2(arr, left, right) {
    let hole = left;
    let key = arr[hole];
    while (left < right) {
        while (left < right && arr[right] >= key) {
            right--;
        }
        a[hole] = arr[right];
        hole = right;
        while (left < right && arr[left] <= key) {
            left++;
        }
        a[hole] = arr[left];
        hole = left;
    }
    a[hole] = key;
    return hole;
}

// 队列[其实差不多，只是利用的队列]
function quickSort3(arr, begin, end) {
    const queue = [];
    if (begin < end) {
        queue.push(begin);
        queue.push(end);
    }
    while (queue.length !== 0) {
        let left = queue.shift();
        let right = queue.shift();
        let mid = partSort2(arr, left, right);
        if (left < mid - 1) {
            queue.push(left);
            queue.push(mid - 1);
        }
        if (right > mid + 1) {
            queue.push(mid + 1);
            queue.push(right);
        }
    }
    console.log(arr);
    return arr;
}
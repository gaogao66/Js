// 回文
function symmetry(num) {
    let arr = [];
    while(num > 0) {
        num == String(num).split("").reverse().join("") ? arr.push(num) : "";
        num--;
    }
    return arr;
}
// console.log(symmetry(1000));

// 不用中间变量置换
function change(a,b) {
    b = b - a;
    a = a + b;
    b = a - b;
    console.log(a,b);
}
// change(1,2);

// 去重

function repeatDel(arr) {
    let set = new Set(arr);
    return Array.from (set);
}
// console.log(repeatDel([1,1,2,2,3,4,5,4,3,3,2,1]));

function repeatDel_second(arr) {
    let hasNum = {};
    let newArr = [];
    arr.forEach(item => {
        !hasNum[item] ? ((hasNum[item] = true), (newArr.push(item))) : "";
        // 或者
        // newArr.indexOf(item) == -1 ? newArr.push(item) : ""; 
        // arr.indexOf(item) == index ? newArr.push(item) : "";
    });
    return newArr;
}
// console.log(repeatDel_second([1,1,2,2,3,4,5,4,3,3,2,1]));

// 统计字符串中重复最多的字母
function getMostRepeat(str) {
    let obj = {};
    let temp = ['', 0];
    str.split("").forEach(item => {
        !obj[item] ? obj[item] = 1 : obj[item]++;
    });
    // for循环
    // for(let i = 0; i < str.length; i++) {
    //     !obj[str.charAt(i)] ? obj[item] = 1 : obj[item]++;
    // }
    Object.entries(obj).forEach(item => {
        item[1] > temp[1] ? ((temp[1] = item[1]), (temp[0] = item[0])) : "";
    });
    return temp;
}
// console.log(getMostRepeat("afjghdfraaaasdenas"));

// 排序

// 找出最大差值
function getMaxProfit(arr) {
    let minNum = arr[0], maxProfit = 0;
    arr.forEach(item => {
        minNum = Math.min(minNum, item);
        let currentProfit = item - minNum;
        maxProfit = Math.max(maxProfit, currentProfit);
    })
    return maxProfit
}
console.log(getMaxProfit([10,5,11,7,8,9]));
// 随机字符串定长
// Math.random().toString(36).substr(2)
// "jdxyur1p6r"
// Math.random()
// 0.3490691399790775
// Math.random().toString(36) 将随机数转换成36进制
// "0.jd47v5wdqa"
// Math.random().toString(36).substr(2)
// "0ll1ptb368jr"
function gerRandomStr(length) {
    let str = "";
    while(str.length < length){
        str += Math.random().toString(36).substr(2)
    }
    return str.substr(0,len);
}

// 对象转数组
// 数组浅拷贝，将类数组或者可遍历对象转换成一个真正的数
Array.from(); 
//将一系列列表转为数组
Array.of();
Array.prototype.slice.call();
// ...Rest参数
// Object方法
Object.keys();Object.values();Object.entries();

// 取数组中最大或最小
Math.max.apply(null, arr);
// 清空数组
// arr = []只是将arr重新赋值，原来的数组没有引用指向它，会等待垃圾回收
let arr = [];
arr.length = 0;
arr.splice(0, arr.length);

// 连续元素最大和
// 1. sum[i][j+1]=sum[i][j]+a[i+j+1]，其中sum[i][j+1]的含义是：
// 从第i个元素开始，长度为j+1个元素的和，再在sum数组中找出元素最大的那个，记录下和，开始序号，长度。n^2+1


// 找出length为n的（[1,2,3,5]）随机数组中,未出现的那个元素
// 空间换时间[,,,,]
// 利用公式 1+2+3+…+n = (1+n) * n / 2
function getNoShowNum(arr) {
    let len = arr.length;
    let res = (len + 1 +1)*(len +1) / 2;
    for (let i = 0; i < arr.length; i++) {
        res -= arr[i];   
    }
    console.log(res);
    return res;
  }
  
  // 对象是最方便查找的数据结构 -- 2n
  function getNoShowNum(arr) {
    let len = arr.length;
    let obj = {};
    while(len > 0) {
        obj[len] = len;
        len--;
    }
    arr.forEach(item => delete obj[item]);
    let result = Object.values(obj);
    console.log(result);
    return result;
  }

// 找出数组中数之和为指定元素的元素组合（2个、3个，n个）元素不可重复
//数据组合 + 回溯

//难点：链式调用 return this
var data = [
    {price: 11, name: "b-123"},
    {price: 21, name: "a-123"},
    {price: 14, name: "b-123"},
    {price: 15, name: "a-123"},
    {price: 12, name: "a-123"},
    {price: 18, name: null}
]

var find = function(origin) {
    this.data = origin;

    this.where = function(obj) {
        let key = Object.keys(obj)[0];
        let val = Object.values(obj)[0];
        this.data = this.data.filter(item => item[key] && item[key].test(val));
        return this;
    }

    this.orderBy = function(key, type) {
        switch(type) {
            case 'desc': return this.data.sort((a, b) => {
                return b[key] - a[key];
            })
            case 'asc': return this.data.sort((a, b) => {
                return a[key] - b[key];
            })
            default: return this.data;
        }
    }

    return this;
}

var result = find(data).where({
    name: /^\a/
})
.orderBy('price', 'desc');

console.log(result);

//排除法
//获取地址栏中的参数并放入对象中

var str = "http://www.baidu.com?id=557131028857&name=ceshi&age=8";
var reg = /([^?=&]+)=([^?=&]+)/g;
var result = {};
str.replace(reg, function($1, $2, $3) {
    result[$2] = $3;
});
console.log(result);

// 给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。
// 注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数。
// 输入：nums = [3,30,34,5,9]
// 输出："9534330"

function getMaxNum(nums) {
    nums.sort((a, b) => {
        let s1 = a + '' + b;
        let s2 = b + '' + a;
        return s2 - s1;
    });
    let result = nums[0] ? nums.join(""): 0;
    console.log(result);
    return result;
}

function getMaxNum(nums) {
    let result = "";
    // 居然才注意到index c = d + 1
    nums.sort((c, d) => {
        let a = String(d), b = String(c);
        if (parseInt(a[0]) < parseInt(b[0]) ) {
            return -1;
        } else if (parseInt(a[0]) === parseInt(b[0])) {
            let str = a.length > b.length ? a : b;
            let index = 1;
            while(index < str.length) {
                if (a[index] !=undefined && b[index] != undefined) {
                    if (a[index] === b[index]) {
                        index++;
                    } else {
                        return a[index] > b[index] ? false : -1; 
                    }
                } else {
                    return (a[index] == undefined && b[index] == 0) || (a[index] > 0 && b[index] == undefined) ? false : -1; 
                }
                
            }
            return false;
        }
    });
    console.log(nums);
    result = nums.join("");
    console.log(result);
    return result;
}
getMaxNum([3,30,34,5,9]);

// sort函数，曾经的版本可能是用过快排和插入在针对不同大小的数组，但此时的sort函数应该是(二分法是查找，应该是归并)
// 且回调参数a, b位置，a代表下一个元素


// 给定一个只包含字符’(’，’)’，’{’，’}’，’[‘和’]'的字符串，判断输入字符串是否有效
let s = '({[]})(}'
function fn(s) {
  if (s.length && s.length % 2 !== 0) {
    return false
  }
  let arr = [];
  s = s.split('')
  for (let i =0;i< s.length; i++) {
    if(arr[arr.length-1] && (s[i] === '}' && arr[arr.length-1]=== '{' || s[i] === ')' && arr[arr.length-1]=== '(' || s[i] === ']' && arr[arr.length-1]=== '[')) {
        arr.pop();
    } else {
        arr.push(s[i]);
    }
  }
  
  return !arr.length ? true : false
}
console.log(fn(s), 'xxx')
// 基础静态类型 优点： 变量类型不可改变，类型的方法和属性也确定
let count: number = 1;
// 自定义静态类型
interface asset {
    ip: string;
    port: number;
}
let asset1: asset = {
    ip: "1.1.1.1",
    port: 3000
}
// 对象类型
// 1.对象 2.数组 3.类 4.函数
let asset2: ()=> string = () => { return "" };

// 类型注解，类型推断
let count1: number;
count1 = 1;
// one,two是any类型，需要类型注解
// function getTotal(one,two) {
//     return one + two;
// }
// const total = getTotal(1,2);

// 函数参数，返回类型定义
function getTotal({one,two}: {one: number, two: number}):number {
    return one + two;
}
const total = getTotal({one: 1,two: 2});

function getOne({one}: {one: number}):number {
    return one;
}
const one = getOne({one: 1});

// 数组类型
const arr: (number | string | undefined)[] = [1, "2", 3, undefined];
// 对象数组——定义类型别名
type assetType = {port: number, ip: string};
const asset3: assetType[] = [{port: 1, ip: ""}]
// 类
class assetType1 {
    ip: string
    port: number
    username!: string // 非必选值
}

// 元组——固定元素类型的位置
const asset: [string, number] = ["", 1];

// interface接口——规范类型

// 1.重复的类型注解
// 类型别名可以直接给类型，但接口必须代表对象
type sameString = string;

interface assetType2 {
    id: number
    ip: string
    port: number
    username?: string // 接口非必选值定义
    [propname: string]: any // 加入任意值——属性名是String类型，属性值any类型
    say(): string // 返回值string类型
}

// 接口和类的约束
class asset4 implements assetType {
    port = 1;
    ip = "";
}
// 接口继承
interface dbAsset extends assetType2 {
    addDBAsset(): string
}

let db1: dbAsset = {
    id: 1,
    ip: "",
    port: 1,
    say: () => {
        return ""
    },
    addDBAsset: () => {
        return ""
    }
}

// 类
// 继承，重写， (类型不可变)
class parent1 {
    level = 1;
    getLevel() {
        return this.level;
    }
}
class child extends parent1 {
    // getLevel() {
    //     return 1 + this.level;
    // }
    getLevel() {
        return super.getLevel() + 1;
    }
    getchild() {
        return "child";
    }
}
// 类访问类型
// private(只允许在类的内部被调用，外部不允许调用)、
// protected(允许在类内及继承的子类中调用)、public(允许在类的内部和外部被调用)
class Person {
    name: string // 默认public 同 public name:string;
    private old: number
    protected color: string
    constructor(public name1:string) {
        this.name = name1;
    }
}
let person = new Person("123");
person.name = "wangjiaer";
console.log(person.name);
// console.log(person.old);
class teacher extends Person {
    public getName() {
        return this.color;
    }
    //子类使用构造函数需要super调用父类构造函数,即便父类没有构造函数，子类也要使用super()进行调用
    constructor(public favorite:number) {
        super("jiaerwang")
    }
}
let li = new teacher(12)
console.log("li",li.name1, li.favorite);
// 类的Getter\setter\static
// private最大用处：封装一个属性，通过getter和setter的形式来访问和修改这个属性
class girl {
    static birth:string = "123"
    public readonly _name:string // 类的只读属性——在实例化时赋予名字，以后不能再更改
    constructor(private _age:number, name:string) {
        this._name = name;
    }
    get age(){
        return this._age;
    }
    set age(age: number) {
        this._age = age;
    }
    static getName() {
        return "baijingting";
    }
}

const miso = new girl(28, "libai");
miso.age = 15;
// miso._name = "dufu";
console.log(miso.age, miso._name);
// static 声明的属性和方法，不需要new声明对象，可以直接使用
console.log(girl.getName(), girl.birth)

// 抽象类abstract,抽象方法也以abstract开头
abstract class watier {
    abstract skill()
}
class base extends watier {
    skill() {
        console.log("初级技能");
    }
}
class high extends watier {
    skill() {
        console.log("高级技能");
    }
}

// 联合类型
const str: string | null = "1";
// 类型保护——类型断言
interface a {
    say:() => {};
}
interface b {
    skill:() => {};
}

function c(val: a | b) {
    // val.skill(); c函数无法准确判断val的类型，需要类型断言
    (val as a).say();
    (val as b).skill();
    //  in语法
    if ("say" in val) {val.say();}
    // typeof
    function add(first: string | number, second: string | number) {
        // return first + second;
        if (typeof first === "string" || typeof second === "string") {
            return `${first}${second}`;
        }
        return first + second;
    }
    // instanceof 保护对象
    class num {
        count: number
    }
    function addObj(first: object | num, second: object | num) {
        if(first instanceof num && second instanceof num ) {
            return first.count + second.count;
        }
        return null;
    }
}
// Enum 枚举类型

//函数泛型——指定联合类型的多个参数为同一类型

function sum<sameType>(first: sameType, second: sameType) {
    return `${first}${second}`;
}
sum <string>("1","2");

// 多个泛型
function sum1<a, b>(first: a, second: b) {
    return `${first}${second}`;
}
sum1 <string, number>("1",2);
// 泛型类型推断
sum1(2,1);

// 泛型继承
interface lastName {
    name: string
}// 泛型约束
class getName<T extends lastName> {
    constructor(private name2: T[]){}
    getLast(index: number) :string {
        return this.name2[index].name;
    }
}


// 22. 括号生成
// 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。

// 暴力解法(回溯+dfs)
// 每个位置有两种可能左括号或者右括号，需要额外判断括号的有效性
// 时间复杂度： O(2^2n * n)
// 空间复杂度： O(n)

var generateParenthesis = function(n) {
    let path = [], result = [], arr = ['(', ')'];
    let isvalidlog = function (str) {
        let stack = [], head, l = '(', r = ')';
        for(let i of str) {
            if (stack.length === 0 && r === i) return false;
            l === i ? (stack.push(i), head = l) : (stack.pop(), head = stack[stack.length - 1]);
        }
        return stack.length ? false : true;
    }
    let makelog = function (arr) {
        if (path.length === 2*n) {
            let str = path.join('');
            // console.log(str, isvalidlog(str))
            isvalidlog(str) ? result.push(str) : null;
            return;
        }
        for(let i of arr) {
            path.push(i);
            makelog(arr);
            path.pop();
        }
    }
    makelog(arr);
    return result;
};

// 优化暴力解法（回溯+dfs）
// 括号成对，右括号个数不能超过左括号

var generateParenthesis = function(n) {
    let result = [];
    let makelog = function (path, left, right) {
        if (left > n || right > left) return;
        if (path.length === 2*n) {
            result.push(path);
            return;
        }
        makelog(path + '(', left+1, right);
        makelog(path + ')', left, right+1);
    }
    makelog('', 0, 0);
    return result;
};

// 自底向上的解法（动态规划）
// 前提是单独拿出来的括号E的左边在N个括号所有排列组合情况中都是处于最左边，所以不存在括号位于括号E的左边的情况, 这个算法主要的基点就是将排列组合的情况分为了括号内和括号外这两种情况，且仅存在两种情况.

// 简单来说，在求N个括号的排列组合时，把第N种情况（也就是N个括号排列组合）视为单独拿一个括号E出来，剩下的N-1个括号分为两部分，P个括号和Q个括号，P+Q=N-1，然后这两部分分别处于括号E内和括号E的右边，各自进行括号的排列组合。由于我们是一步步计算得到N个括号的情况的，所以小于等于N-1个括号的排列组合方式我们是已知的,且P+Q=N-1，P和Q是小于等于N-1的，所以我们能直接得到P个和Q个括号的情况，进而得到N个括号的结果！

var generateParenthesis = function(n) {
    let dp = [];
    dp[0] = [''];
    dp[1] = ['()'];
    for(let i = 2; i <= n; i++) {
        dp[i] = [];
        for(let p = i-1,q = 0; p > -1; p--, q++) {
            let qlen = dp[q].length;
            while(qlen > 0) {
                qlen--;
                let plen = dp[p].length;
                while(plen > 0) {
                    plen--;
                    dp[i].push(`(${dp[q][qlen]})${dp[p][plen]}`);
                }
            }
        }
    }
    console.log(dp)
    return dp[n];
};

const hash = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
};

var romanToInt = function(s) {
    let result = 0, front = -Infinity;
    for(let i = s.length - 1; i > -1; i--) {
        // 由于罗马数字的规范，只有4，9，90，40， 900， 400等情况
        // 才会出现左小于右，且字符有且仅有为两个
        // 那么当出现左小于右的情况就减去左边数字，其他情况为加，记录前一个字符
        if (front > hash[s[i]]) {
            result -= hash[s[i]];
        } else {
            result += hash[s[i]];
        }
        front = hash[s[i]];
    }
    return result;
};
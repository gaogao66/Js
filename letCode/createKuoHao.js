// 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。

// 方法1: 回溯
function generateParenthesis(n) {
    // write code here
    const [left, right] = ['(', ')'];
    const arr = [left, right];
    const result = [], path = [];
    const fn = function (path, n) {
        let lc = 0, rc = 0, i = 0;
        while (i < path.length) {
            (path[i] === left) && lc++;
            (path[i] === right) && rc++;
            i++;
        }
        if (lc < rc || path.length > 2 * n) return;
        if ((path.length === 2 * n) && (lc === rc)) {
            let str = path.join('');
            result.push(str);
            return;
        }
        for (let i = 0; i < 2; i++) {
            path.push(arr[i]);
            fn(path, n);
            path.pop();
        }
    }
    fn(path, n);
    return result;
}

// 方法2: dp
var generateParenthesis = function (n) {
    let dp = [];
    dp[0] = [''];
    dp[1] = ['()'];
    for (let i = 2; i <= n; i++) {
        dp[i] = [];
        for (let p = i - 1, q = 0; p > -1; p--, q++) {
            let qlen = dp[q].length;
            while (qlen > 0) {
                qlen--;
                let plen = dp[p].length;
                while (plen > 0) {
                    plen--;
                    dp[i].push(`(${dp[q][qlen]})${dp[p][plen]}`);
                }
            }
        }
    }
    console.log(dp);
    return dp[n];
};
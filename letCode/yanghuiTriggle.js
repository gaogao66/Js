// 杨辉三角

var generate = function (n, m) {
    const result = [];
    result[0] = [1];
    let count = 0;
    let numRows = n;
    while (count < numRows - 1) {
        result[count + 1] = [];
        let len = result[count].length;
        for (let i = 0; i < len; i++) {
            let num = result[count][i] + (result[count][i - 1] || 0);
            result[count + 1].push(num);
        }
        result[count + 1].push(result[count][len - 1]);
        count++;
    }
    return result[n - 1][m - 1];
};

var generate = function (n, m) {
    const result = [];
    result[0] = [1];
    let count = 0;
    let numRows = n;
    while (count < numRows - 1) {
        result[count + 1] = [];
        let len = result[count].length;
        for (let i = 0; i < len && (result[count + 1].length < Math.ceil((count + 2) / 2)); i++) {
            let num = result[count][i] + (result[count][i - 1] || 0);
            result[count + 1].push(num);
        }
        result[count + 1].push(...result[count + 1].slice(0, (count + 2) % 2 ? result[count + 1].length - 1 || result[count + 1].length : result[count + 1].length).reverse())
        count++;
    }
    return result[n - 1][m - 1];
};

console.log(generate(7, 5))

var generate = function (numRows) {
    const arr = [];
    let n = 0;
    while (n < numRows) {
        if (n === 0) {
            arr[n] = [1];
        } else {
            arr[n] = [];
            // 第几行几个数字
            for (let i = 0; i < n + 1; i++) {
                // 核心代码
                arr[n][i] = (arr[n - 1][i - 1] || 0) + (arr[n - 1][i] || 0);
            }
        }
        n++;
    }
    return arr[n];
};
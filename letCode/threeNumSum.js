var threeNumSum = function(nums) {
    const result = [];
    nums.sort((a, b) => a - b);
    for(let i = 0; i < nums.length; i++) {
        if (nums[i] > 0) break;
        if (i > 0 && nums[i-1] === nums[i]) continue;
        let l = i+1, r = nums.length - 1;
        while(l < r) {
            if (nums[i] + nums[l] + nums[r] === 0) {
                result.push([nums[i], nums[l], nums[r]]);
                while(l < r && nums[l] === nums[l+1]) l++;
                while(l < r && nums[r] === nums[r-1]) r--;
                l++;
                r--;
            } else if (nums[i] + nums[l] + nums[r] > 0) {
                r--;
            } else {
                l++;
            }
        }
    }
    return result;
}

console.log(threeNumSum([-1, -1, 0, 1]))
// # 149. 直线上最多的点数
// > 题目： 给你一个数组 points ，其中 points[i] = [xi, yi] 表示 X-Y 平面上的一个点。求最多有多少个点在同一条直线上。

// > 思路： 利用基准点和斜率（y1-y1/x1-x2）,两层循环，每次将与基准点构成的不同斜率存在hash中，相同则计数，一趟下来求max。
// > 注意点：需要区别y1-y2 === 0 和 x1-x2 === 0 的情况。

// 时间复杂度： O（n^2）
// 空间复杂度：O（n）

// > 1. 在**点的总数量小于等于 2** 的情况下，我们总可以用一条直线将所有点串联，此时我们直接返回点的总数量即可；
// > 2. 当我们找到**一条直线经过了图中超过半数的点**时，我们即可以确定该直线即为经过最多点的直线；
// > 3. 当我们枚举到点 i（假设编号从 0 开始）时，我们至多只能找到 n−i 个点共线。假设此前找到的共线的点的数量的最大值为 k，如果有 k≥n−i，那么此时我们即可停止枚举，因为不可能再找到更大的答案了。


var maxPoints = function(points) {
    // 经过某点的斜率
    let max = 0;
    // 优化1
    if (points.length <= 2) return points.length;
    for(let i = 0; i < points.length; i++) {
    // 优化2
        if (max > Math.floor(points.length / 2) || max >= points.length - i) {
            break;
        }
        const hash = {};
        for(let j = i+1; j < points.length; j++) {
            const k = points[i][0]-points[j][0] === 0 ? '+0' : (points[i][1]-points[j][1])/(points[i][0]-points[j][0]);
            hash[k] ? hash[k]++ : hash[k] = 2;
        };
        for(let i in hash) {
            max = Math.max(max, hash[i]);
        }
    }
    return max;
};
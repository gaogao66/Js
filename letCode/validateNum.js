// 4. 数字判断

// 以下字符串是合法的十进制数字：
// 0
// 123
// 1.23
// -123
// 1.23E+10
// 以下字符串是不合法的十进制数字：
// 000
// 123abc
// .123
// 0.1.2

var strAsTenNum = function (s) {
    const State = {
        INITIAL: 'INITIAL',
        ITEGER: 'ITEGER',
        INT_SIGN: 'INT_SIGN',
        POINT: 'POINT',
        FRACTION: 'FRACTION',
        EXP: 'EXP',
        EXP_SIGN: 'EXP_SIGN',
        EXP_NUMBER: 'EXP_NUMBER',
    }

    const CharType = {
        CHAR_NUMBER: 'CHAR_NUMBER',
        CHAR_E: 'CHAR_E',
        CHAR_SIGN: 'CHAR_SIGN',
        CHAR_POINT: 'CHAR_POINT',
        CHAR_ILLEGAL: 'CHAR_ILLEGAL'
    }

    const toCharType = function (code) {
        if (!isNaN(code)) {
            return CharType.CHAR_NUMBER;
        } else if (code.toLowerCase() === 'e') {
            return CharType.CHAR_E;
        } else if (code === '.') {
            return CharType.CHAR_POINT;
        } else if (code === '+' || code === '-') {
            return CharType.CHAR_SIGN;
        } else {
            return CharType.CHAR_ILLEGAL;
        }
    }

    const dealHash = function () {
        const hash = new Map();
        const InitialMap = new Map();
        InitialMap.set(CharType.CHAR_SIGN, State.INT_SIGN);
        InitialMap.set(CharType.CHAR_NUMBER, State.ITEGER);
        hash.set(State.INITIAL, InitialMap);
        const IntSignMap = new Map();
        IntSignMap.set(CharType.CHAR_NUMBER, State.ITEGER);
        hash.set(State.INT_SIGN, IntSignMap);
        const IntegerMap = new Map();
        IntegerMap.set(CharType.CHAR_NUMBER, State.ITEGER);
        IntegerMap.set(CharType.CHAR_E, State.EXP);
        IntegerMap.set(CharType.CHAR_POINT, State.POINT);
        hash.set(State.ITEGER, IntegerMap);
        const PointMap = new Map();
        PointMap.set(CharType.CHAR_NUMBER, State.FRACTION);
        hash.set(State.POINT, PointMap);
        const FractionMap = new Map();
        FractionMap.set(CharType.CHAR_NUMBER, State.FRACTION);
        FractionMap.set(CharType.CHAR_E, State.EXP);
        hash.set(State.FRACTION, FractionMap);
        const ExpMap = new Map();
        ExpMap.set(CharType.CHAR_SIGN, State.EXP_SIGN);
        ExpMap.set(CharType.CHAR_NUMBER, State.EXP_NUMBER);
        hash.set(State.EXP, ExpMap);
        const expSignMap = new Map();
        expSignMap.set(CharType.CHAR_NUMBER, State.EXP_NUMBER);
        hash.set(State.EXP_SIGN, expSignMap);
        const ExpNumberMap = new Map();
        ExpNumberMap.set(CharType.CHAR_NUMBER, State.EXP_NUMBER);
        hash.set(State.EXP_NUMBER, ExpNumberMap);
        return hash;
    }

    const hash = dealHash();
    const len = s.length;
    let state = State.INITIAL;
    const EndType = new Map();
    EndType.set(State.ITEGER, State.ITEGER);
    EndType.set(State.FRACTION, State.FRACTION);
    EndType.set(State.EXP_NUMBER, State.EXP_NUMBER);

    const index = toCharType(s[0]) === CharType.CHAR_SIGN ? true : false ? 1 : 0;
    const isZero = (parseInt(s[index]) === 0) && (toCharType(s[index+1]) === CharType.CHAR_NUMBER);

    for(let i = 0; i < len; i++) {
        const type = toCharType(s[i]);
        if (hash.get(state).has(type)) {
            state = hash.get(state).get(type);
        } else {
            return false;
        }
    }

    return (EndType.has(state) ? true : false) && !isZero;
}

console.log(strAsTenNum('0e3312'));



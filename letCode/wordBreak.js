// 139.单词拆分
// 动态规划

var wordBreak = function(s, wordDict) {
    const dp = Array(s.length+1).fill(false);
    dp[0] = true;
    const hash = {};
    for(let k of wordDict) {
        hash[k] = true;
    }
    for(let i = 1; i <= s.length; i++) {
        for(let j = 0; j < i; j++) {
            let str = s.substring(j, i);
            if (dp[j] && hash[str]) {
                dp[i] = true;
                break;
            }
        }
    }
    console.log(dp)
    return dp[s.length];
};

// dfs + memory记录计算过的（dp）

var wordBreak = function(s, wordDict) {
    // memory(从start开始，组合成功的走到边界的dp[start]才=true)
    const dp = Array(s.length);
    const hashSet = new Set(wordDict);
    var canBreak = function (start) {
        // start移动到最后一个元素边界，成功找到结束递归
        if (s.length === start) return true;
        if (dp[start] !== undefined) return dp[start];
        for(let i = start+1; i <= s.length; i++) {
            let str = s.substring(start, i);
            if (hashSet.has(str) && canBreak(i)) {
                dp[start] = true;
                return true;
            }
        }
        dp[start] = false;
        return false;
    }
    return canBreak(0);
    console.log(dp);
};

// bfs

var wordBreak = function(s, wordDict) {
    // visited表示当前节点访问过
    const visted = Array(s.length);
    const hashSet = new Set(wordDict);
    // 维护一个队列，依然用指针描述一个节点, start。
    const queue = [0];
    while(queue.length !== 0) {
        // 取队首元素，为start开始下标
        let start = queue.shift();
        if (visted[start]) continue;
        visted[start] = true;
        for(let i = start+1; i <= s.length; i++) {
            let str = s.substring(start, i);
            if (hashSet.has(str)) {
                if (i < s.length) {
                    queue.push(i);
                } else {
                    return true;
                }
            } 
        }
    }
    return false;
};


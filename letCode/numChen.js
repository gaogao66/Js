// 43. 大数相乘

// 解法1: 按照列竖式的思路，遍历 num2 每一位与 num1 进行相乘，将每一步的结果进行累加。
var multiply = function (num1, num2) {
    let result = '0';
    for (let i = num1.length - 1, j = 0; i > -1; i--, j++) {
        let tempResult = '', front = 0;
        for (let k = num2.length - 1; k > -1; k--) {
            let temp = (num1[i] || 0) * (num2[k] || 0) + front;
            tempResult = temp % 10 + tempResult;
            front = Math.floor(temp / 10);
        }
        // 最后的进位存在
        if (front) tempResult = front + tempResult;
        // 运算的结果向后补0
        tempResult += Array(j).fill(0).join('');
        // 处理高位为0的情况
        tempResult = tempResult.replace(/^0+/, '');
        console.log(tempResult, result)
        result = bigAdd(result, tempResult);
    }
    return result;
};

var bigAdd = function (num1, num2) {
    let result = '', front = 0;
    let len = Math.max(num1.length, num2.length);
    while (num1.length < len) {
        num1 = '0' + num1;
    }
    while (num2.length < len) {
        num2 = '0' + num2;
    }
    for (let i = len - 1; i > -1; i--) {
        let temp = num1[i] * 1 + num2[i] * 1 + front;
        result = temp % 10 + result;
        front = Math.floor(temp / 10);
    }
    // 最后的进位存在
    if (front) result = front + result;
    return result;
}

console.log(multiply("4251760288481937956",
    "765115951945318380788262319277966797284599232062230077200"));

// 解法2: 优化竖式
// 该算法是通过两数相乘时，乘数某位与被乘数某位相乘，与产生结果的位置的规律来完成。具体规律如下：

// 乘数 num1 位数为 M，被乘数 num2 位数为 N， num1 x num2 结果 res 最大总位数为 M + N
// num1[i] x num2[j] 的结果为 tmp(位数为两位，"0x","xy" 的形式)，其第一位位于 res[i + j]，第二位位于 res[i + j + 1]。

var multiply = function (num1, num2) {
    let res = [];
    for (let i = num1.length - 1; i > -1; i--) {
        for (let j = num2.length - 1; j > -1; j--) {
            let temp = (res[i + j + 1] || 0) + num1[i] * num2[j];
            res[i + j + 1] = temp % 10;
            res[i + j] = (res[i + j] || 0) + Math.floor(temp / 10);
        }
    }
    let result = res.join('').replace(/^0+/, '') || '0';
    return result;
}

// 虽然两者时间复杂度和空间复杂度相同，但优化竖式执行速度提高很明显，普通竖式耗时主要还是对每步计算的字符串相加这个过程。
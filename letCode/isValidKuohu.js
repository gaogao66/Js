// 给定一个只包含字符’(’，’)’，’{’，’}’，’[‘和’]'的字符串，判断输入字符串是否有效

//  方法1: 利用栈 code1
let s = '({[]})(}'
function fn(s) {
    if (s.length && s.length % 2 !== 0) {
        return false
    }
    let arr = [];
    s = s.split('')
    for (let i = 0; i < s.length; i++) {
        // 栈顶和当前的匹配判断
        if (arr[arr.length - 1] && (s[i] === '}' && arr[arr.length - 1] === '{' || s[i] === ')' && arr[arr.length - 1] === '(' || s[i] === ']' && arr[arr.length - 1] === '[')) {
            arr.pop();
        } else {
            arr.push(s[i]);
        }
    }

    return !arr.length ? true : false;
}

// 方法1: 利用栈 code2
var match = function (a, b) {
    let left = ['(', '[', '{'];
    let right = [')', ']', '}'];
    let index = 0;
    for (let i = 0; i < left.length; i++) {
        if (a === left[i] && right[i] === b) {
            return true;
        }
    }
    return false;
}
var isValid = function (s) {
    let stack = [];
    let top = null;
    for (let i of s) {
        match(top, i) ? stack.pop() : stack.push(i);
        top = stack[stack.length - 1];
    }
    return stack.length ? false : true;
};

// 方法1: 利用栈 code3
function isValid(s) {
    const left = {
        "(": 1,
        "{": 2,
        "[": 3,
    },
        right = {
            ")": 1,
            "}": 2,
            "]": 3,
        };
    const stack = [];
    for (let i = 0; i < s.length; i++) {
        if (left[s[i]]) {
            stack.push(s[i]);
        } else {
            let temp = stack[stack.length - 1];
            if (right[s[i]] === left[temp]) {
                stack.pop();
            } else {
                return false;
            }
        }
    }
    return stack.length === 0 ? true : false;
}

// 12. 整数转罗马数字
const hash = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M'
};

var transToRoman = function (num) {
    let result = '';
    if (hash[num]) return hash[num];
    // 处理4，9，40，90，400，900, 或者直接写入字典种
    let len = num.toString().length - 1;
    let bu = Math.pow(10, len);
    let newNum = num + bu;
    if (num && hash[newNum]) {
        result = hash[bu] + hash[newNum];
        return result;
    }
    while (num !== 0) {
        let key = '';
        for (let i in hash) {
            if (Number(i) > num) {
                break;
            }
            key = i;
        }
        result += hash[key];
        num -= key;
        console.log(num, result)
    }
    console.log(result)
    return result;
};

var intToRoman = function (num) {
    let str = num + '';
    let result = '';
    for (let i = str.length - 1, j = 0; i > -1; i--, j++) {
        let tempNum = str[i] * Math.pow(10, j);
        result = transToRoman(tempNum) + result;
    }
    return result;
};

console.log(intToRoman(1994));

// 方式2

const hash1 = {
    1: 'I',
    4: 'IV',
    5: 'V',
    9: 'IX',
    10: 'X',
    40: 'XL',
    50: 'L',
    90: 'XC',
    100: 'C',
    400: 'CD',
    500: 'D',
    900: 'CM',
    1000: 'M'
}

var transToRoman1 = function (s) {
    let result = '';
    if (hash1[s]) return hash1[s];
    while (s !== 0) {
        let key;
        for (let i in hash1) {
            if (Number(i) > s) {
                break;
            }
            key = i;
        }
        result += hash1[key];
        s -= key;
    }
    return result;
}

var intToRoman1 = function (num) {
    let str = num + '';
    let result = '';
    for (let i = str.length - 1, j = 0; i > -1; i--, j++) {
        let tempNum = str[i] * Math.pow(10, j);
        result = transToRoman1(tempNum) + result;
    }
    return result;
};

console.log(intToRoman1(1994));

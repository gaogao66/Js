// 思路： 将问题转化为：子数组【0， i】的最大和连续子数组。
// dp数组存放nums[i]之前的最大和，dp[i+1] = Math.max(dp[i-1]+nums[i], nums[[i])
// 时间复杂度： O(n)
var maxSubArray = function(nums) {
    let max = nums[0];
    for(let i = 1; i < nums.length; i++) {
        nums[i] = Math.max(nums[i-1]+nums[i], nums[i]);
        max = Math.max(max, nums[i]);
    }
    return max;
};

// 分治法
class Status {
    constructor(tSum,lSum, rSum, mSum) {
        this.tSum = tSum;
        this.lSum = lSum;
        this.rSum = rSum;
        this.mSum = mSum;
    }
}
const maxSubSubArray = function(nums, l, r) {
    if (l === r) {
        return new Status(nums[l], nums[l], nums[l], nums[l]);
    }
    let mid = (l + r) >> 1;
    let left = maxSubSubArray(nums, l, mid);
    let right = maxSubSubArray(nums, mid+1, r);
    return new Status(
        left.tSum + right.tSum,
        Math.max(left.lSum, left.tSum+right.lSum),
        Math.max(right.rSum, right.tSum+left.rSum),
        Math.max(Math.max(left.mSum, right.mSum), left.rSum + right.lSum)
    );
}
var maxSubArray = function(nums) {
    return maxSubSubArray(nums, 0, nums.length-1).mSum;
};


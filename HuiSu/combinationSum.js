// 全排列

// 1. 广度优先遍历
// 2. 深度优先遍历
// 回溯

// 39. 组合总和(arr不含重复元素，可以重复)

// 回溯
var combinationSum = function (candidates, target) {
    let result = [];
    let len = candidates.length;
    // 路径
    let path = [];
    if (len == 0) return result;
    // 优化： 排序数组
    candidates.sort((a, b) => a - b);
    fn(candidates, 0, target, path);
    console.log(result);
    return result;

    function fn(candidates, start, target, path) {
        if (target < 0) return;
        if (target == 0) {
            result.push([...path]);
            return;
        }
        // start避免重复，只向后找
        for (let i = start; i < candidates.length; i++) {
            if (target >= candidates[i]) {
                path.push(candidates[i]);
                fn(candidates, i, target - candidates[i], path);
                // 回退一步
                path.pop();
            }
        }
    }
};

// let candidates = [2,3,6,7], target = 7;
combinationSum(candidates, target);

// 动态规划
var combinationSum = function (candidates, target) {
    let dp = [];
    for (let i = 1; i <= target; i++) {
        dp[i] = [];
        for (let j = 0; j < candidates.length; j++) {
            if (i == candidates[j]) {
                dp[i].push([candidates[j]]);
            }
            if (i > candidates[j]) {
                for (let k = 0; k < dp[i - candidates[j]].length; k++) {
                    dp[i].push([...dp[i - candidates[j]][k], candidates[j]].sort())
                }
            }
        }
        // 去重dp[i]
        dp[i] = noRepeat(dp[i]);
    }
    console.log(dp[target]);
    return dp[target];
};

function noRepeat(arr) {
    let map = new Map();
    let array = new Array();  // 数组用于返回结果
    for (let i = 0; i < arr.length; i++) {
        if (map.has(arr[i].join(''))) {  // 如果有该key值
            map.set(arr[i], true);
        } else {
            map.set(arr[i].join(''), false);   // 如果没有该key值
            array.push(arr[i]);
        }
    }
    return array;
}

// 40.组合总和2(arr含重复元素，不可重复)

var combinationSum = function (candidates, target) {
    let result = [];
    let path = [];
    // 排序
    candidates.sort((a, b) => a - b);
    fn(candidates, target, path, 0);
    console.log(result);
    return result;
    function fn(candidates, target, path, start) {
        if (target == 0) {
            result.push([...path].sort());
            return;
        }
        for (let i = start; i < candidates.length; i++) {
            // 去重关键，重复的不去计算(i > start)
            if (i > start && candidates[i] == candidates[i - 1]) continue;
            if (candidates[i] <= target) {
                path.push(candidates[i]);
                fn(candidates, target - candidates[i], path, i + 1);
                path.pop();
            } else {
                break;
            }
        }
    }
};


let candidates = [10, 1, 2, 7, 6, 1, 5], target = 8;
combinationSum(candidates, target);
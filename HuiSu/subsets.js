// 78. 子集（arr无重复元素）

// 1. 遍历result数组，每一次，基础上新增元素，push进result
var permuteUnique = function (nums) {
    const result = [];
    for (let i of nums) {
        let len = result.length;
        while (len) {
            len--;
            let temp = [...result[len]];
            temp.push(i);
            result.push(temp);
        }
        result.push([i]);
    }
    result.push([]);
    console.log(result);
    return result;
};
permuteUnique([1, 2, 3])

// 2. 回溯
var permuteUnique = function (nums) {
    const result = [];
    const path = [];
    fn(nums, 0);
    console.log(result);
    return result;
    function fn(nums, start) {
        result.push([...path]);
        for (let i = start; i < nums.length; i++) {
            path.push(nums[i]);
            fn(nums, i + 1);
            path.pop();
        }
    }
};
permuteUnique([1, 2, 3])

// 90. 子集II(arr中有重复元素)

var permuteUnique = function (nums) {
    const result = [];
    const path = [];
    // 加个排序
    nums.sort();
    fn(nums, 0);
    console.log(result);
    return result;
    function fn(nums, start) {
        result.push([...path]);
        // 记录同一层的元素，重复元素就跳过
        let obj = {};
        for (let i = start; i < nums.length; i++) {
            if (obj[nums[i]]) continue;
            obj[nums[i]] = true;
            path.push(nums[i]);
            fn(nums, i + 1);
            path.pop();
        }
    }
};
permuteUnique([4, 4, 4, 1, 4])
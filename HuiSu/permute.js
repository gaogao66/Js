// 46. 全排列(arr不含重复元素)

// 回溯
var permute = function (nums) {
    let result = [];
    let path = [];
    fn(nums);
    console.log(result);
    return result;
    function fn(nums) {
        if (nums.length === 0) {
            result.push([...path]);
            return;
        }
        for (let i = 0; i < nums.length; i++) {
            path.push(nums[i]);
            // 删除当前push的元素
            let temp = nums.slice();
            temp.splice(i, 1);
            fn(temp);
            path.pop();
        }
    }
};

permute([1, 2, 3]);

// 47. 全排列II (arr含重复元素)
var permuteUnique = function (nums) {
    const result = [];
    const path = [];
    fn(nums);
    console.log(result);
    return result;
    function fn(nums) {
        if (nums.length === 0) {
            result.push([...path]);
            return;
        }
        // 利用obj存储当前层的元素，有重复则跳过当前循环
        const obj = {};
        for (let i = 0; i < nums.length; i++) {
            // 1
            if (obj[nums[i]]) continue;
            // 2
            obj[nums[i]] = true;
            path.push(nums[i]);
            let temp = nums.slice(0);
            temp.splice(i, 1);
            fn(temp);
            path.pop();
        }
    }
};
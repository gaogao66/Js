// 层遍历
var levelOrder = function(root) {
    let arr = [];
    let result = [];
    // 记录父元素个数，null个数
    let parentNum = 1;
    for(let i = 0; i < root.length; i++) {
        if(!root[i]) {
            parentNum--;
            if (!parentNum) {
                parentNum = arr.length;
                result.push(arr.splice(0, arr.length))
            }
        } else {
            arr.push(root[i])
        }
    }
    result.push([...arr]);
    console.log(result);
    return result;
};
let root = [1,null,3,2,4,null,5,6];
levelOrder(root);

// node结构
function Node(val,children) {
    this.val = val;
    this.children = children;
};

class changeStyle{
	constructor(btnObj, divObj, json) {
		this.btnObj = btnObj;
		this.divObj = divObj;
		this.json = json;
	}
	// 封装，继承，多态
	init() {
		this.btnObj.onclick = function() {
			for(let key in this.json) {
				this.divObj.style[key] = this.json[key]; 
			}
		}
	}
}
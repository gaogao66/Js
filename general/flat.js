// 1. ...
// 二维变一维
Array.prototype.concat.apply([], [1,2,3,[4,5]]);
[].concat(...[1,2,3,[4,5]]);
// 多维
function flatten(arr) {
  while(arr.some(item => Array.isArray(item))) {
    arr = [].concat(...arr);
  }
  return arr;
}

// 2. 递归

function flatten1(arr) {
  let result = [];
  arr.map(item => {
    if (Array.isArray(item)) {
      result = result.concat(flatten1(item));
    } else {
      result.push(item);
    }
  })
  return result;
}

// join split
// 注释
// var arr = [1,2,[3,[4,[5]]]];
// arr.join(",");
// "1,2,3,4,5"
// arr.toString()
// "1,2,3,4,5"
function flatten2(arr) {
  return arr.join(",").split(",").map(item => parseInt(item));
}

// toString split
function flatten3(arr) {
  return arr.toString().split(",").map(item => parseInt(item));
}

// reduce
function flatten4(arr) {
  return arr.reduce((result, item) => {
    return result.concat(Array.isArray(item) ? flatten4(item) : item);
  }, [])
}

function flatten5(arr) {
  // 扩展运算符实现扁平化
  // while(arr.some(item => Array.isArray(item))) {
  //   arr = [].concat(...arr);
  // }
  // return arr;
  // 递归
  // let result = [];
  // arr.map(item => {
  //   if(Array.isArray(item)) {
  //     result = result.concat(flatten5(item));
  //   } else {
  //     result.push(item);
  //   }
  // })
  // return result;
  // reduce
  // return arr.reduce((result, item) => {
  //   return result.concat(Array.isArray(item) ? flatten5(item) : item);
  // }, []);
}


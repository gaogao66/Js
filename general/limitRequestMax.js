const reqs = new Array(10).fill(false).map((_, i) =>
    () => new Promise(resolve => setTimeout(() => { console.log(i); resolve(i) }, 1000)))
// 要求：实现executeSerial函数，功能如下：接受一个数组，数组的每一项是一个返回promise的函数，要在第一个函数返回的promise resolve后，再执行第二个。

const executeSerial = (reqs) => {
    if (reqs.length <= 0) return;

    const req = reqs.shift();
    req().finally(_ => {
        executeSerial(reqs)
    })
}

executeSerial(reqs)

const executeSerialMax = (reqs, max) => {
    let curRequestCount = 0;

    const run = function () {
        if (!reqs.length) return;

        while (reqs.length && curRequestCount < max) {
            // console.log(new Date())
            const task = reqs.shift();
            curRequestCount++;
            task().then().finally(_ => {
                curRequestCount--;
                run();
            })
        }
    }

    run();
}

executeSerialMax(reqs, 3)
let iterator = myIterator([1,2]);
function myIterator(arr) {
  let index = 0;
  return {
    next() {
      return index < arr.length ? 
      {value: arr[index++], done: false} :
      {value: undefined, done: true}
    }
  }
}
console.log( iterator.next() ); // {value: 3, done: false}

console.log( iterator.next() ); // {value: 7, done: false}

console.log( iterator.next() ); // {value: undefined, done: true}
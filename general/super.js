// 继承

// 父类
function parent(name) {
  this.name = name;
  this.hobbies = ['1', '2'];
  this.sum = function() {
    alert(this.name);
  }
}
parent.prototype.age = 10;

// 1. 原型链继承
// 重点：让子类的原型等于父类的实例
// 特点：子类实例可继承的属性：实例的构造函数，父类的构造函数，父类的原型属性
// 缺点：子类实例无法向父类构造函数传参；继承单一；所有新实例都会共享父类原型属性（构造函数属性是引用类型的话，会共享引用类型）
function child() {
  this.name = "child";
}
child.prototype = new parent();

// let child1 = new child();
// let child2 = new child();
// console.log(child1.age); // 10
// child1.age = 20;
// console.log(child1.age, parent.prototype.age); // 20, 10
// child1.hobbies.push('3');
// console.log(child1.hobbies, child2.hobbies); //[ '1', '2', '3' ] [ '1', '2', '3' ] 构造函数属性是引用类型的话，会共享引用类型
// console.log(child1 instanceof parent); // true

// 2. 借用构造函数继承
// 特点：这里跟原型链继承有个比较明显的区别是并没有使用prototype继承，而是在子类里面执行父类的构造函数。
// 相当于把父类的代码复制到子类里面执行一遍，这样做的另一个好处就是可以给父类传参。
// 缺点：函数也是引用类型，也没办法共享了。
// 即每个实例里面的函数，虽然功能一样，但是却不是一个函数，就相当于我们每实例化一个子类，就复制了一遍函数代码。

// 特点：只继承了父类构造函数属性，没有继承父类的原型属性，解决了原型继承缺点，可继承多个构造函数属性，子类可向父类传参
// 缺点：只能继承父类构造函数属性，无法实现构造函数的复用（每个实例都重新调用），每个新实例都有父类构造函数副本

function child(name) {
  parent.call(this, name); // 解决引用类型被所有实例共享, 可以给父类传参
}

// let child1 = new child();
// let child2 = new child();
// child1.hobbies.push('3');
// console.log(child1.hobbies, child2.hobbies);// [ '1', '2', '3' ] [ '1', '2' ]
// console.log(child1.sum === child2.sum);  // false

// 总结：

// 原型继承	Student.prototype = new Person()	实例的引用类型共享
// 构造函数继承	在子类(Student)里执行Person.call(this)	实例的引用类型不共享

// 3. 组合继承
// 特点：普通属性使用构造函数继承（传参），函数使用原型链继承（复用）
// 缺点：调用两次父类构造函数（耗内存）
function parent() {
  this.hobbies = ['1', '2'];
}
parent.prototype.sum = () => {};
function child() {
  parent.call(this); // 构造函数继承属性
}
child.prototype = new parent(); // 原型继承方法(将方法写在原型上)

// let child1 = new child();
// let child2 = new child();
// child1.hobbies.push('3');
// console.log(child1.hobbies, child2.hobbies);// [ '1', '2', '3' ] [ '1', '2' ]
// console.log(child1.sum === child2.sum);  // true

// 4. 原型式继承
// 重点：用一个函数包装一个对象，然后返回这个函数的调用，这个函数就变成可以随意添加属性的实例或对象，Object.create()就是这个原理
// 特点：类似复制一个对象，用函数来包装
// 缺点：所有实例都会继承原型上的属性；无法实现复用（新实例属性都是后面添加的）

// 先封装一个函数容器，用户输出对象和承载继承的原型
function content(obj) {
  function F() {}
  F.prototype = obj;// 继承了传入的参数
  return new F(); // 返回函数实例对象
}

// let sup = new parent();
// let sup1 = content(sup);

// 5. 寄生式继承
// 重点：给原型式继承外面套了个壳子
// 优点：没有创建自定义类型
// 缺点：没用到原型？无法复用

function content(obj) {
  function F() {}
  F.prototype = obj;
  return new F();
}
let sup = new parent();
// 以上是原型式继承，加个壳子传递参数
function child(obj) {
  let sub = content(obj);
  sub.name = "11";
  return sub;
}
let sup1 = child(sup);
// 这个函数经过声明就成了可增添属性的对象
console.log(sup1.name); // 11, 返回了sub对象，继承了sub属性

// 6. 寄生组合式继承（常用）
// 重点：修复了组合继承
// 最简
function inheritPrototype(child, parent){
  var prototype = Object.create(parent.prototype);       //创建对象
  prototype.constructor = child;                   //增强对象
  child.prototype = prototype;                     //指定对象
}

function content(obj) {
  function F() {}
  F.prototype = obj;
  return new F();
}
// prototype实例（F实例）的原型继承了父类的函数原型，更像原型链继承，只不过只继承了原型属性
let prototype = content(parent.prototype);
function child() {
  parent.call(this); // 这个继承父类构造函数属性
} // 解决了组合式两次调用构造函数的缺点
// 重点
child.prototype = prototype; // 继承prototype
prototype.constructor = child; // 修复实例

// es6 extends（语法糖，和寄生组合一样）
class parent {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  toString() {
    return '(' + this.x + ', ' + this.y + ')';
  }
}
class child extends parent {
}


// 工厂模式
function personFactory(name) {
    var obj = new Object();
    obj.name = name;
    obj.showName = function() {
        return this.name;
    }
    return obj;
}
var p = personFactory("libai");
// 缺点：不知道由那个函数构造

// 构造函数模式
function personFactory1(name) {
    this.name = name;
    this.showName = function() {
        return this.name;
    }
}
var p1 = new personFactory1("libai");
// 被观察者
class Observerd {
    constructor(name) {
        this.name = name;
        this.state = "走路";
        this.observers = [];
    }

    setObserver(observer) {
        this.observers.push(observer)
    }

    setState(state) {
        this.state = state;
        this.observers.forEach(observer => observer.update(this))
    }
}
// 观察者
class Observer {
    constructor() { }
    update(observerd) {
        console.log(observerd.name + "正在" + observerd.state)
    }
}

// const observer = new Observer();
// const observerd = new Observerd('lili');
// observerd.setObserver(observer);
// observerd.setState('xuexi');
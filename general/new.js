function newObj(fn, ...rest) {
    let obj = {};
    obj.__proto__ = fn.prototype;
    fn.apply(obj, rest);
    return obj;
  }
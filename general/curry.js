// // // 函数柯里化
// // function sum(...rest) {
// //     let arr = rest;
// //     let _sum = function() {
// //         arr.concat(rest);
// //         return _sum;
// //     }
// //     return _sum;
// //     return {
// //         sumof: function() {
// //             return arr.reduce((a,b) => {
// //                 return a+ b;
// //             });
// //         }
// //     }
// // }
// function sum(fn, args) {
//     var _this = this
//     var len = fn.length;

//     var args = args || [];
//     return function() {
//         var _args = Array.prototype.slice.call(arguments);
//         Array.prototype.push.apply(args, _args);
//         // 如果参数个数小于最初的fn.length，则递归调用，继续收集参数
//         if (_args.length < len) {
//             return progressCurrying.call(_this, fn, _args);
//         }
//         // 参数收集完毕，则执行fn
//         // return fn.apply(this, _args);
//         return {
//             sumof: function () {
//                 return _args.reduce((a,b) => {
//                     return a+b;
//                 })
//             }
//         }
//     }
// }

// let count = sum(1,2,3).sumof;
// console.log(count);
// let count1 = sum(1)(2)(3).sumof;
// console.log(count1);
// function curry(fn, curArgs) {
//     return function() {
//         let args = [].slice.call(arguments);
//         if(curArgs !== undefined) {
//             args = args.concat(curArgs);
//         }
//         if(args.length < fn.length) {
//             return curry(fn, args);
//         }
//         return fn.apply(null,args);
//     }
// }
// function sum(...rest) {
//     return rest.reduce((a,b) => {
//         return a+b;
//     })
// } 
// const fn = curry(sum);

// console.log(fn(1,2,3));
// console.log(fn(1,2)(3));
// console.log(fn(1)(2,3));
// console.log(fn(1)(2)(3));
let add = (...rest) =>  {
    let arr = rest;
    // 相当于参数 fn 及 return 语句之前定义的变量都被保存下来了
    let next = (...args) => {
        if (args.length > 0) {
            arr.push(...args);
            // 实现链式调用
            return next;
        } else {
            return {
                sumof: function() {
                    return arr.reduce((a,b) => {
                        return a+b;
                    }, 0)
                }
            };
        }
    }
    return next;
    // ES5 写法
    // return function() {
    //     if (arguments.length === 0) {
    //         return fn.call(null, arr);
    //     } else {
    //         arr = arr.concat([].slice.call(arguments));
    //     }
    // }

}


console.log(add(1,2,3)().sumof());
console.log(add(1,2)(3)().sumof());
console.log(add(1)(2,3)().sumof());
console.log(add(1)(2)(3)().sumof());


function add(x) {
    var sum = x;

    function tem(num) {
        sum += num;
        return tem;
    }
    // 隐式类型转换
    tem.valueOf = () => {
        return sum;
    }

    return tem;
}

console.log(+add(1)(2)(3));

// 最后一个函数的处理

// 实现一个add方法，使计算结果能够满足如下预期：
// add(1)(2)(3) = 6;
// add(1, 2, 3)(4) = 10;
// add(1)(2)(3)(4)(5) = 15;

function add() {
    // 第一次执行时，定义一个数组专门用来存储所有的参数
    var _args = Array.prototype.slice.call(arguments);

    // 在内部声明一个函数，利用闭包的特性保存_args并收集所有的参数值
    var _adder = function() {
        _args.push(...arguments);
        return _adder;
    };

    // 利用toString隐式转换的特性，当最后执行时隐式转换，并计算最终的值返回
    _adder.toString = function () {
        return _args.reduce(function (a, b) {
            return a + b;
        });
    }
    return _adder;
}

console.log(+add(1)(2)(3))                // 6
console.log(+add(1, 2, 3)(4))             // 10
console.log(+add(1)(2)(3)(4)(5))          // 15
console.log(+add(2, 6)(1))                // 9


// 将函数的实际参数转换成数组的方法

// 方法一：var args = Array.prototype.slice.call(arguments);
// 方法二：var args = [].slice.call(arguments, 0);

// toString() 和 valueOf() 

// 1.toString()返回的是字符串，而valueOf()返回的是原始值，没有原始值返回对象本身
// 2.undefined和null都没有toString()和valueOf()方法
// 3.Date类型的toString()返回的表示时间的字符串；valueOf()返回的是现在到1970年1月1日的毫秒数
// 4.Number类型的toString()方法可以接收转换基数，返回不同进制的字符串形式的数值；而valueOf()方法无法接受转换基数
// 5.Object.prototype.toString.toString()能够很好的判断数据的类型及内置对象


function add() {
    let arr = Array.prototype.slice.call(arguments);

    function args() {
        arr.push(...arguments);
        return args;
    }

    args.toString = () => {
        return arr.reduce((a,b) => {
            return a+b;
        }, 0);
    }

    return args;
}

function curry(fn) {
    let len = fn.length;
    var arr = Array.prototype.slice.call(arguments);

    return function() {
        var _arr = [].slice.call(arguments);
        arr.unshift(_arr);

        if (arr.length < len) {
            return curry(fn);
        }

        return fn.call(...arr);
    }
}


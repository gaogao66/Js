// 链表的奇偶重排

// 方法1: 奇偶两个链表存，最后相连
// 运行超时——【暂时不清楚超时原因】
// 空间复杂度 O(n)，时间复杂度 O(n)
function oddEvenList(head) {
    let oddHead = new ListNode(), evenHead = new ListNode(), odd = 0;
    let cur = head, oddList = oddHead, evenList = evenHead;
    const res = evenHead;
    while (cur) {
        if (!odd) {
            odd = 1;
            evenList.next = cur;
            evenList = evenList.next;
        } else {
            odd = 0;
            oddList.next = cur;
            oddList = oddList.next;
        }
        cur = cur.next;
    }
    evenList.next = oddHead.next;
    return res.next;
}

// 方法2: 双指针
// 空间复杂度 O(1)，时间复杂度 O(n)
// 具体做法：
// step 1：判断空链表的情况，如果链表为空，不用重排。
// step 2：使用双指针odd和even分别遍历奇数节点和偶数节点，并给偶数节点链表一个头。
// step 3：上述过程，每次遍历两个节点，且even在后面，因此每轮循环用even检查后两个元素是否为NULL，如果不为再进入循环进行上述连接过程。
// step 4：将偶数节点头接在奇数最后一个节点后，再返回头部
function oddEvenList(head) {
    if (!head) return head;
    let odd = head, even = head.next, evenHead = head.next;
    while (even && even.next) {
        odd.next = even.next;
        odd = odd.next;
        even.next = odd.next;
        even = even.next;
    }
    odd.next = evenHead;
    return head;
}

module.exports = {
    oddEvenList: oddEvenList
};
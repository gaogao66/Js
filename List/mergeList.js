//  合并两个排序的链表
// 时间复杂度O(n),空间复杂度 O(1)

import ListNode from './listNode.js';

function Merge(pHead1, pHead2) {
    let head = new ListNode(0), temp = head;
    while (pHead1 && pHead2) {
        if (pHead1.val <= pHead2.val) {
            temp.next = pHead1;
            pHead1 = pHead1.next;
        } else {
            temp.next = pHead2;
            pHead2 = pHead2.next;
        }
        temp = temp.next;
    }
    temp.next = pHead1 || pHead2;
    return head.next;
}
module.exports = {
    Merge: Merge,
};
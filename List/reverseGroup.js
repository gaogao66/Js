// 链表中的节点每k个一组翻转
// 空间复杂度 O(1)，时间复杂度 O(n)

/**
  * 
  * @param head ListNode类 
  * @param k int整型 
  * @return ListNode类
  */
function reverseKGroup(head, k) {
    // write code here
    let pre = head, front = head, odd = 1;
    while (pre) {
        let c = k, temp = pre;
        while (--c && temp) {
            temp = temp.next;
        }
        if (temp) {
            c = k;
            let curTemp = null, newF = pre;
            while (c--) {
                let t = pre.next;
                pre.next = curTemp;
                curTemp = pre;
                pre = t;
            }
            odd === 1 ? (odd = 2, head = curTemp) : (front.next = curTemp, front = newF);
        } else {
            odd === 1 ? odd = 2 : (front.next = pre);
            return head;
        }
    }
    return head;
}
module.exports = {
    reverseKGroup: reverseKGroup
};
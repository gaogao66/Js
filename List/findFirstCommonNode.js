// 两个链表的第一个公共结点
// 时间复杂度O(n),空间复杂度 O(1)

// 方法1: 先把第一个链表的节点全部存放到集合set中，然后遍历第二个链表的每一个节点，判断在集合set中是否存在，如果存在就直接返回这个存在的结点。如果遍历完了，在集合set中还没找到，说明他们没有相交，直接返回null即可
// 时间复杂度O(M + N)：M, N分别表示 pHead1， pHead2的链表长度，最差情况下需要遍历完两个链表
// 空间复杂度O(M)：需要额外集合空间存储 pHead1 结点
function FindFirstCommonNode(pHead1, pHead2) {
    // write code here
    const hash = new Set();
    while (pHead1) {
        hash.add(pHead1);
        pHead1 = pHead1.next;
    }
    while (pHead2) {
        if (hash.has(pHead2)) {
            return pHead2;
        }
        pHead2 = pHead2.next;
    }
    return null;
}

// 方法2: 先统计两个链表的长度，如果两个链表的长度不一样，就让链表长的先走，直到两个链表长度一样，这个时候两个链表再同时每次往后移一步，看节点是否一样，如果有相等的，说明这个相等的节点就是两链表的交点
// 时间复杂度O(2M + 2N)：M, N分别表示 pHead1， pHead2的链表长度
// 空间复杂度O(1)

// 方法3: 使用两个指针 a，b 分别指向两个链表 pHead1，pHead2的头结点，然后同时分别逐结点遍历，当 a 到达链表 pHead1的末尾时，重新定位到链表 pHead2的头结点；当 b 到达链表 pHead2 的末尾时，重新定位到链表 pHead1的头结点。当双指针相遇时，所指向的结点就是第一个公共结点;没有公共节点，最终都会走向结束null
// 时间复杂度O(M + N),空间复杂度O(1)

function FindFirstCommonNode(pHead1, pHead2) {
    // write code here
    let p = pHead1, q = pHead2;
    while (p !== q) {
        p = !p ? pHead2 : p.next;
        q = !q ? pHead1 : q.next;
    }
    return p;
}
module.exports = {
    FindFirstCommonNode: FindFirstCommonNode
};
// 链表——反转
// 时间复杂度O(n),空间复杂度 O(1)

function ReverseList(head) {
    let pre = null, cur = head;
    while (cur) {
        let temp = cur.next;
        cur.next = pre;
        pre = cur;
        cur = temp;
    }
    return pre;
}

module.exports = {
    ReverseList: ReverseList
}

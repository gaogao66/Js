// 删除有序链表中重复的元素-II

// 方法1: 哈希表【适用于无序和有序】
// 空间复杂度 O(n)，时间复杂度 O(n)
// 具体做法：
// step 1：遍历一次链表用哈希表记录每个节点值出现的次数。
// step 2：在链表前加一个节点值为0的表头，方便可能的话删除表头元素。
// step 3：再次遍历该链表，对于每个节点值检查哈希表中的计数，只留下计数为1的，其他情况都删除。
// step 4：返回时去掉增加的表头。
function deleteDuplicates(head) {
    const hash = {};
    const res = new ListNode(0);
    res.next = head;
    let node = head;
    while (node) {
        if (hash[node.val]) {
            hash[node.val]++;
        } else {
            hash[node.val] = 1;
        }
        node = node.next;
    }
    let pre = res, cur = head;
    while (cur) {
        if (hash[cur.val] > 1) {
            pre.next = cur.next;
        } else {
            pre = pre.next;
        }
        cur = cur.next;
    }
    return res.next;
}

// 方法2: 直接比较删除
// 空间复杂度 O(1)，时间复杂度 O(n)
// 遍历链表，每次比较相邻后两个节点，如果遇到了两个相邻节点相同，则新开内循环将这一段所有的相同都遍历过去
function deleteDuplicates(head) {
    const res = new ListNode(0);
    res.next = head;
    let cur = res;
    while (cur.next && cur.next.next) {
        if (cur.next.val === cur.next.next.val) {
            let repeat = cur.next.val;
            while (cur.next && repeat === cur.next.val) {
                cur.next = cur.next.next;
            }
        } else {
            cur = cur.next;
        }
    }
    return res.next;
}
module.exports = {
    deleteDuplicates: deleteDuplicates,
};
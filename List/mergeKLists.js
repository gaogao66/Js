// 合并k个已排序的链表
// 时间复杂度 O(nlogn)

import Merge from './mergeList';

function mergeKLists(lists) {
    let resHead = null;
    for (let i = 1; i < lists.length; i++) {
        let [pHead1, pHead2] = [lists[i - 1], lists[i]];
        lists[i] = Merge(pHead1, pHead2);
    }
    resHead = lists[lists.length - 1];
    return resHead;
}
// 分治+递归
function mergeKLists(lists) {
    if (lists.length <= 1) return lists[0];
    return mergeList(lists, 0, lists.length - 1);
}
function mergeList(lists, left, right) {
    if (left === right) return lists[left];
    const middle = Math.floor((left + right) / 2);
    return merge(mergeList(lists, left, middle), mergeList(lists, middle + 1, right))
}
function merge(l1, l2) {
    let result = new ListNode(0), temp = result;
    while (l1 && l2) {
        if (l1.val <= l2.val) {
            temp.next = l1;
            l1 = l1.next;
        } else {
            temp.next = l2;
            l2 = l2.next;
        }
        temp = temp.next;
    }
    temp.next = l1 || l2;
    return result.next;
}
module.exports = {
    mergeKLists: mergeKLists
};
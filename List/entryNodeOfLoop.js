// 链表中环的入口结点
// 时间复杂度O(n),空间复杂度 O(1)

// 方法1：hash法，记录第一次重复的结点
// 通过使用set或者map来存储已经遍历过的结点，当第一次出现重复的结点时，即为入口结点。
function EntryNodeOfLoop(pHead) {
    const set = new Set();
    while (pHead) {
        if (set.has(pHead)) return pHead;
        set.add(pHead);
        pHead = pHead.next;
    }
    return null;
}
// 方法2：快慢指针
// 通过定义slow和fast指针，slow每走一步，fast走两步，若是有环，则一定会在环的某个结点处相遇（slow == fast），
// 根据下图分析计算，可知从相遇处到入口结点的距离与头结点与入口结点的距离相同。
function EntryNodeOfLoop(pHead) {
    let slow = pHead, fast = pHead;
    while (fast && fast.next) {
        slow = slow.next;
        fast = fast.next.next;
        if (slow === fast) break;
    }
    if (!fast || !fast.next) return null;
    fast = pHead;
    while (fast !== slow) {
        fast = fast.next;
        slow = slow.next;
    }
    return fast;
}
module.exports = {
    EntryNodeOfLoop: EntryNodeOfLoop
};
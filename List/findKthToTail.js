// 链表中倒数最后k个结点
// 时间复杂度O(n),空间复杂度 O(1)

// 双指针
function FindKthToTail(pHead, k) {
    let slow = pHead, fast = pHead;
    while (k-- && fast) {
        fast = fast.next;
    }
    if (k > -1) return null;
    while (fast) {
        slow = slow.next;
        fast = fast.next;
    }
    return slow;
}
module.exports = {
    FindKthToTail: FindKthToTail
};
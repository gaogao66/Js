//  删除链表的倒数第n个节点
// 时间复杂度O(n),空间复杂度 O(1)

function removeNthFromEnd(head, n) {
    // write code here
    // 双指针 || 遍历一遍
    let slow = head,
        fast = head;
    while (n-- && fast) {
        fast = fast.next;
    }
    if (!fast) return head.next;
    while (fast?.next) {
        slow = slow.next;
        fast = fast.next;
    }
    slow.next = slow.next?.next;
    return head;
}
module.exports = {
    removeNthFromEnd: removeNthFromEnd,
};
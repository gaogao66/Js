import ListNode from "./listNode";

export function ReverseBetween(head: ListNode, m: number, n: number): ListNode {
    let count = 1;
    let newHead: ListNode | null = head, front: ListNode | null = null;
    while (newHead && count !== m) {
        front = newHead;
        newHead = newHead.next;
        count++;
    }
    let pre: ListNode | null = null, cur = newHead, end: ListNode = cur;
    while (cur && count <= n) {
        let temp = cur.next;
        cur.next = pre;
        pre = cur;
        cur = temp;
        count++;
    }
    end.next = cur;
    if (front) {
        front.next = pre;
        return head;
    }
    return pre;
}
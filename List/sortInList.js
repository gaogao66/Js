// 单链表的排序

// 方法1: 将排序转到数组中
// 时间复杂度O(NlogN)：N表示链表结点数量，遍历链表O(N)，数组排序(NlogN)，遍历数组O(N)
// 空间复杂度O(N)：使用额外数组占用空间O(N)

// 方法2: 归并（递归）
// 时间复杂度O(NlogN)：N表示链表结点数量，二分归并算法O(NlogN)
// 空间复杂度O(1)：仅使用常数级变量空间
// 注意：使用 fast,slow 快慢双指针法，奇数个节点找到中点，偶数个节点找到中心左边的节点。找到中点 slow 后，执行 slow.next = None 将链表切断。
function sortInList(head) {
    // write code here
    if (!head || !head.next) return head;
    // 使用快慢指针寻找链表的中点
    let fast = head.next, slow = head;
    while (fast && fast.next) {
        slow = slow.next;
        fast = fast.next.next;
    }
    let middle = slow.next;
    slow.next = null;
    let left = sortInList(head);
    let right = sortInList(middle);
    // 合并left、right
    const res = new ListNode(0);
    let temp = res;
    while (left && right) {
        if (left.val < right.val) {
            temp.next = left;
            left = left.next;
        } else {
            temp.next = right;
            right = right.next;
        }
        temp = temp.next;
    }
    temp.next = left || right;
    return res.next;
}
module.exports = {
    sortInList: sortInList
};
// 判断链表中是否有环
// 时间复杂度O(n),空间复杂度 O(1)

function hasCycle(head) {
    let fast = head, slow = head;
    while (fast && fast.next) {
        slow = slow.next;
        fast = fast.next.next;
        if (slow === fast) return true;
    }
    return false;
}
module.exports = {
    hasCycle: hasCycle
};
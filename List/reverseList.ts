import ListNode from "./listNode";

export function ReverseList(head: ListNode): ListNode {
    if (!head) return head;
    let pre: ListNode | null = null, cur: ListNode | null = head;
    while (cur) {
        let temp = cur.next;
        cur.next = pre;
        pre = cur;
        cur = temp;
    }
    return pre ? pre : new ListNode();
}
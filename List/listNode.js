export default function ListNode(val, next) {
    this.val = val;
    this.next = next || null;
}
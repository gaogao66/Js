// 链表——指定区间反转
// 时间复杂度O(n),空间复杂度 O(1)

function reverseBetween(head, m, n) {
    // write code here
    let count = 1;
    let newHead = head, front = null;
    while (newHead && count !== m) {
        front = newHead;
        newHead = newHead.next;
        count++;
    }
    let pre = null, cur = newHead, end = cur;
    while (cur && count <= n) {
        let temp = cur.next;
        cur.next = pre;
        pre = cur;
        cur = temp;
        count++;
    }
    end.next = cur;
    if (front) {
        front.next = pre;
        return head;
    }
    return pre;
}

module.exports = {
    reverseBetween: reverseBetween
};
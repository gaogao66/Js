// 链表相加(二)
// 空间复杂度 O(n)，时间复杂度 O(n)

// 方法1: 反转链表
// 空间复杂度 O(1)，时间复杂度 O(n)
function addInList(head1, head2) {
    if (!head1) return head2;
    if (!head2) return head1;
    head1 = reverseList(head1);
    head2 = reverseList(head2);
    let tem = 0;
    let res = new ListNode(0);
    node = res;
    while (head1 || head2) {
        let val = tem;
        if (head1) {
            val += head1.val;
            head1 = head1.next;
        }
        if (head2) {
            val += head2.val;
            head2 = head2.next;
        }
        tem = Math.floor(val / 10);
        node.next = new ListNode(val % 10);
        node = node.next;
    }
    if (tem) node.next = new ListNode(tem);
    res = reverseList(res.next);
    return res;
}

function reverseList(head) {
    let pre = null, cur = head;
    while (cur) {
        let tem = cur.next;
        cur.next = pre;
        pre = cur;
        cur = tem;
    }
    return pre;
}

// 方法2: 使用辅助栈 + 头插法
// 空间复杂度 O(n)，时间复杂度 O(n)
function addInList(head1, head2) {
    if (!head1) return head2;
    if (!head2) return head1;
    const stack1 = [], stack2 = [];
    let p1 = head1, p2 = head2;
    while (p1) {
        stack1.push(p1);
        p1 = p1.next;
    }
    while (p2) {
        stack1.push(p2);
        p2 = p2.next;
    }
    const res = new ListNode(-1);
    let tem = 0;
    while (stack1.length || stack2.length) {
        let val = tem;
        if (stack1.length) {
            val += stack1.pop().val;
        }
        if (stack2.length) {
            val += stack2.pop().val;
        }
        tem = Math.floor(val / 10);
        let node = new ListNode(val % 10);
        node.next = res.next;
        res.next = node;
    }
    if (tem > 0) {
        let node = new ListNode(tem);
        node.next = res.next;
        res.next = node;
    }
    return res.next;
}

module.exports = {
    addInList: addInList
};
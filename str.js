// 3. 无重复字符的最长子串
var lengthOfLongestSubstring = function(s) {
    let max = 0;
    for(let i = 0; i < s.length; i++) {
        let len = 1, flag = false;
        for(let j = i+1; j < s.length; j++) {
            len = j - i;
            let str = s.substr(i, len);
            if (str.indexOf(s[j]) !== -1) {
                flag = false;
                break;
            }
            flag = true;
        }
        if(flag) len++;
        len > max ? max = len : null;
    }
    console.log(max)
    return max;
};
lengthOfLongestSubstring(" ")

// 7. 整数反转
var reverse = function(x) {
    let f = x < 0 ? -1 : 1;
    let str = x.toString();
    f < 0 && (str = str.slice(1));
    str = str.split('').reverse().join('');
    str.replace(/^0+/, '');
    if (!str) return 0;
    str *= f;
    str = str >= (-2) ** 31 && str <= 2**31 - 1 ? str : 0;
    return str;
};
reverse(123); 
// 8. 字符串转整数
var myAtoi = function(s) {
    if (!s || !s.length) return 0;
    // 删除前面的空白字符
    s = s.replace(/^\s+/, "");
    let flag = 1, i = 0,res = 0;
    // 判断第一位
    if(s[0] === '-' || s[0] === '+') {
        if(s[0] === '-') flag = -1;
        i++;
    } else if (/[^0-9]/.test(s[0])) return 0*flag;
    for(;i < s.length; i++) {
        // if (Number.isNaN(Number(s[i]))) return res*flag; // Number(' ') === 0
        if (/[^0-9]/.test(s[i])) return res*flag;
        // 按位计算res值
        res = res*10 + Number(s[i]);
        if (flag > 0 && res > 2**31 - 1) return 2**31 - 1;
        if (flag < 0 && res > 2**31) return (-2)**31;
    }
    return res*flag;
};
myAtoi(
"-91283472332")
// 一次编辑
// 字符串有三种编辑操作:插入一个字符、删除一个字符或者替换一个字符。 
// 给定两个字符串，编写一个函数判定它们是否只需要一次(或者零次)编辑。
var oneEditAway = function(first, second) {
    let len = Math.abs(first.length - second.length);
    if (len <= 1) {
        if (first.length < second.length) {
            return oneEditAway(second,first);
        }
        let count = 0;
        for(let i = 0; i < first.length; i++) {
            if (first[i] !== second[i - len * count] && ++count > 1) {
                return false;
            }
        }
        return true;
    } 
    return false;
};
// # 给定一个只包含数字的字符串，复原它并返回所有可能的 IP 地址格式。
// 输入: "25525511135"
// 输出: ["255.255.11.135", "255.255.111.35"]

// 思路： ip地址的范围在0.0.0.0-255.255.255.255,即str长度须在4-12之间，
// 回溯递归，每个位置上的数字长度有1，2，3种, 避免重复的情况是在剩余str字符不足的情况不进行递归

var computedIp = function (arr, str, path, startIndex, result) {
    let rest = str.length - startIndex;
    if (path.length === 4) {
        rest <= 0 ? result.push(path.join('.')) : null;
        return;
    }
    for(let i = 0; i < arr.length; i++) {
        let flag = 0;
        let num = rest < arr[i] ? (str.substr(startIndex, rest), flag = 1) : str.substr(startIndex, arr[i]);
        if (!num || Number(num) > 255) {
            return;
        }
        path.push(num);
        !flag ? computedIp(arr, str, path, startIndex+arr[i], result) : null;
        path.pop();
    }
}

var ipAddress = function (str) {
    let result = [];
    let len = str.length;
    let positionNumLens = [1, 2, 3];
    let path = [];
    if (len < 4 || len > 12) return result;
    computedIp(positionNumLens, str, path, 0, result);
    return result;
}

console.log(ipAddress('23232323'))
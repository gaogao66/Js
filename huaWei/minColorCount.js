// 同种颜色的所有数都可以被这个颜色中最小的那个数整除
// 现在帮小朋友们算算最少需要多少种颜色，给这N个数进行上色

var minColorCount = function (str) {
    let arr = str.split(' ').map(i => Number(i)).sort((a, b) => a - b);
    let result = [];
    for(let i = 0; i < arr.length; i++) {
        let temp = [arr[i]];
        for(let j = i+1; j < arr.length; j++) {
            arr[j] % arr[i] === 0 ? (temp.push(arr[j]), arr.splice(j, 1), j--) : null;
        }
        result.push([...temp]);
    }
    console.log(result)
    return result.length;
}

console.log(minColorCount('5 2 3'))
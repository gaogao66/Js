// 题目32 窗口滑动所产生所以窗口和的最大值
// 有一个N个整数的数组, 和一个长度为M的窗口
// 窗口从数组内的第一个数开始滑动,直到窗口不能滑动为止
// 每次滑动产生一个窗口  和窗口内所有数的和
// 求窗口滑动产生的所有窗口和的最大值

var moveWindowMaxSum = function (str, m) {
    let arr = str.split(' ').map(i => Number(i));
    let sumMax = 0, sum = 0;
    let left = right = 0;
    for(let i = 0; i < arr.length; i++) {
        sum += arr[i];
        let count = right - left + 1;
        while(count > 3) {
            sum -= arr[left];
            left++;
            count--;
        }
        sumMax = Math.max(sumMax, sum);
        right++;
    }
    return sumMax;
}

console.log(moveWindowMaxSum('12 10 20 30 15 23', 3))

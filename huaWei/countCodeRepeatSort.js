// 输入: xyxyXX
// 输出:x:2;y:2;X:2;

var tongJSort = function (str) {
    let obj = {};
    // 统计
    for(let i of str) {
        obj[i] ? obj[i]++ : obj[i] = 1;
    }
    let arr = [];
    // 插入排序
    for(let k in obj) {
        arr.push({key: k, val: obj[k]});
        for(let i = arr.length - 1; i > 0 ; i--) {
            if (arr[i-1].val < arr[i].val) {
                [arr[i-1], arr[i]] = [arr[i], arr[i-1]];
            } else if (arr[i-1].val === arr[i].val) {
                let fascii = arr[i-1].key.charCodeAt();
                let bascii = arr[i].key.charCodeAt();
                // 都是大写或小写，前面的比后面的大； 或者前面的是大写后面小写
                if ((((fascii > 90 && bascii > 90 || fascii < 91 && bascii < 91) && fascii > bascii) || 
                (fascii < 91 && bascii > 90)) ) {
                    [arr[i-1], arr[i]] = [arr[i], arr[i-1]];
                } else {
                    break;
                }   
            } else {
                break;
            }
        }
    }
    // 拼接结果
    return arr.map(item => `${item.key}:${item.val};`).join('');
}

console.log(tongJSort('aabbbbxyYxYYyXX'));
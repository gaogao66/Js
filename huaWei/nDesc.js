// 有一个数列A[n]
// 从A[0]开始每一项都是一个数字
// 数列中A[n+1]都是A[n]的描述
// 其中A[0]=1
// 规则如下
// A[0]:1
// A[1]:11 含义其中A[0]=1是1个1 即11
// 表示A[0]从左到右连续出现了1次1
// A[2]:21 含义其中A[1]=11是2个1 即21
// 表示A[1]从左到右连续出现了2次1
// A[3]:1211 含义其中A[2]从左到右是由一个2和一个1组成 即1211
// 表示A[2]从左到右连续出现了一次2又连续出现了一次1
// A[4]:111221  含义A[3]=1211 从左到右是由一个1和一个2两个1 即111221
// 表示A[3]从左到右连续出现了一次1又连续出现了一次2又连续出现了2次1

var getNextDesc = function (str) {
    let count = 0, code = str[0];
    let result = '';
    for(let i of str) {
        code === i ? count++ : (result += count + code, code = i, count = 1);
    }
    result += count + code;
    return result;
}

var desc = function (n) {
    let arr = ['1'];
    let i = 1;
    while(i <= n) {
        arr[i] = getNextDesc(arr[i-1]);
        i++;
    }
    console.log(arr);
    return arr[arr.length-1];
}

console.log(desc(4));
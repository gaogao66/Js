// 单词接龙的规则是: 
// 可用于接龙的单词首字母必须要与前一个单词的尾字母相同
// 当存在多个首字母相同的单词时
// 取长度最长的单词
// 如果长度也相等则取词典序最小的单词
// 已经参与接龙的单词不能重复使用

// 现给定一组全部由小写字母组成的单词数组,并指定其中的一个单词为起始单词,进行单词接龙
// 请输出最长的单词串, 单词串是由单词拼接而成 中间没有空格

var wordConnect = function (startIndex, n, dictory) {
    let result = '';
    while(startIndex > -1) {
        result += dictory[startIndex];
        dictory.splice(startIndex, 1);
        startIndex = findIndex(dictory, result[result.length-1]);
        console.log(startIndex)
    }
    return result;
};

var findIndex = function (dictory, firstWord) {
    let index = -1, len = 0;
    for(let i = 0; i < dictory.length; i++) {
        dictory[i][0] === firstWord ? dictory[i].length > len ? 
        (index = i, len = dictory[i].length) : dictory[i].length === len ? dictory[i] < dictory[index] ? index = i : null : null : null;
    }
    return index;
}

console.log(wordConnect(4, 6, ["word", "dd", "da", "dc", "dword", "d"]));

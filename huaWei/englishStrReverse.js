// 英语字符部分反转
// 关键点在于反转的元素下标计算 end - i + start 和 Math.floor((end - start + 1) / 2) + start
// 时间复杂度3n
var englishStrReverse = function (str, start, end) {
    let arr = str.split(/\s+/);
    if (arr.length-1 < end) return 'EMPTY';
    let index = Math.floor((end - start + 1) / 2) + start;
    for(let i = start; i < index; i++) {
        [arr[i], arr[end]] = [arr[end], arr[i]];
        end--;
    }
    return arr.join(' ');
}

// 时间复杂度2n
var englishStrReverse = function (str, start, end) {
    let arr = str.split(/\s+/);
    if (arr.length-1 < end) return 'EMPTY';
    let result = '', reverseStr = '';
    for(let i = 0; i < arr.length; i++) {
        i >= start && i <= end ? reverseStr += arr[end-i+start] + ' ' : result += arr[i] + ' ';
        i === end ? result += reverseStr : null; 
    }
    console.log(result)
    return result.substring(0, result.length - 1);
}

console.log(englishStrReverse('I am a developer.', 0, 3))
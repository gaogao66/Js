// 用数组代表每个人的能力
// 一个比赛活动要求 参赛团队的最低能力值为N
// 每个团队可以由一人或者两人组成
// 且一个人只能参加一个团队
// 计算出最多可以派出多少只符合要求的队伍

var groupSkillVal = function (str, n) {
    let arr = str.split(' ').map(i => Number(i)).sort((a, b) => {
        return a - b;
    });
    // 双指针法
    let left = 0, right = arr.length - 1;
    let result = [], sum = arr[right], path = [];
    while(left < right) {
        if (arr[right] >= n) {
            result.push([arr[right]])
        } else {
            let sum = arr[right];
            while(left < right) {
                sum += arr[left];
                if (sum >= n) {
                    result.push([arr[left], arr[right]]);
                    break;
                } else {
                    sum -= arr[left];
                    left++;
                }
            }
        }
        right--;
    }
    console.log(result);
    return result.length;
}

console.log(groupSkillVal('3 1 5 7 9 2 6', 8))
// 题目1 请求出给定n m 范围内所有的勾股数元组
// 如果三个正整数A B C ,A²+B²=C²则为勾股数
// 如果ABC之间两两互质，即A与B A与C B与C均互质没有公约数，
// 则称其为勾股数元组。
// 请求出给定n m 范围内所有的勾股数元组
// 输入描述
// 起始范围 1<n<10000    n<m<10000
// 输出目描述
// abc 保证a<b<c输出格式  a b c
// 多组勾股数元组 按照a升序b升序 c升序的排序方式输出。
// 给定范围内，找不到勾股数元组时，输出  Na

// checkHZ 和 checkGY 一样的功能
var checkHZ = function (a, b) {
    let c = 0;
    a < b ? [a, b] = [b, a] : null;
    while((c = a % b) !== 0) {
        a = b;
        b = c;
    }
    return b;
}

// 互质没有公约数
var checkGY = function (a, b) {
    // 0 和任意数没有公约数
    if (a == 0 || b == 0) {
        return 1;
    }
    // 每次a和b取余不等于0时，将b当作a,b设置为a % b传入递归，直至取余为0时， b为最大公约数
    if (a % b === 0) {
        return b;
    } else {
        return checkGY(b, a % b);
    }

}

var gouguNumGroup = function(n,m) {
    let result = [];
    for(let i = n; i <= m; i++) {
        for(let j = i+1; j <= m; j++) {
            let k = i*i + j*j;
            let tem = Math.floor(Math.sqrt(k));
            if (k === tem*tem && tem <= m){
                k = tem;
                checkGY(k, i) === 1 && checkGY(k, j) === 1 && checkGY(i, j) === 1
                ? result.push([k, i, j].sort((a, b) => a - b))
                : null;
            }
        }
    }
    return result.length ? result : 'Na';
}

console.log(gouguNumGroup(5, 10));
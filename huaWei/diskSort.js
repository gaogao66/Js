// #  52磁盘容量排序(ok)

// 磁盘的容量单位有M,G,T这三个等级，他们之间的换算关系为
// 1T=1024G
// 1G=1024M
// 现在给定N块磁盘的容量，请对他们按从小到大的顺序进行稳定排序
// 例如给定5块盘容量：1T,20M,3G,10G6T,3M12G9M
// 排序后的结果为     20M,3G,3M12G9M,1T,10G6T

// 注意单位可以重复出现
// 上述3M12G9M为 3M+12G+9M和 12M12G相等

// 输入描述:
// 输入第一行包含一个整数N，2<=N<=100 ,表示磁盘的个数
// 接下来的N行每行一个字符串 长度 (2<l<30)表示磁盘的容量，有一个或多个格式为 mv的子串组成
// 其中m表示容量大小 v表示容量单位
// 例如

// 磁盘容量m的范围 1~1024正整数容量单位v的范围包含题目中提到的M,G,T

var changeDanWei = function (str) {
    let reg = /(\d+)([T|M|G])/g;
    let arr = str.match(reg);
    let result = 0;
    console.log(arr)
    for(let i of arr) {
        let tem = /(\d+)([T|M|G])/g.exec(i);
        let num = Number(tem[1]);
        let dan = tem[2];
        switch (dan) {
            case 'T': num *= 1024;break;
            case 'M': num /= 1024;
        }
        result += num;
    }

    return result;
}

var diskSort = function (arr) {
    let result = [], hash = {};
    for(let i = 0; i < arr.length; i++) {
        hash[arr[i]] = changeDanWei(arr[i]);
    }
    for(let k in hash) {
        result.push(k);
        for(let j = result.length - 1; j > 0; j--) {
            if (hash[result[j]] < hash[result[j-1]]) {
                [result[j-1], result[j]] = [result[j], result[j-1]];
            }
        }
    }
    return result;
}

console.log(diskSort(['1T','20M','3G','10G6T','3M12G9M']))
console.log(diskSort(['2G4M', '3M2G', '1T']))



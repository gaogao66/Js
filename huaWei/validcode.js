// 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
var match = function (a, b) {
    let left = ['(', '[', '{'];
    let right = [')', ']', '}'];
    for(let i = 0; i < left.length; i++) {
        // a只能是左括号，b只能是右括号的情况才匹配成功
        if (a === left[i] && right[i] === b) {
             return true;
        }
    }
    return false;
}
var isValid = function(s) {
    let stack = [];
    let top = null;
    for(let i of s) {
        console.log(stack);
        match(top, i) ? stack.pop() : stack.push(i);
        top = stack[stack.length-1];
    }
    return stack.length ? false : true;
};

console.log(isValid("([)]"));
// 给出一个数组nums，你需要通过交换位置，将数组中 任何位置 上的 小于 k 的整数 组合到一起，并返回所有可能中所需最少的交换次数。

// 输出将数组A中小于k的整数组合到一起的最小交换次数。

// 组合在一起是指满足条件的数字相邻，不要求在数组中的位置相邻。

// 滑动窗口的思想
// 1. 根据终点状态（交换后，1 都连在一起），逆推检查过程，所有 1 连在一起就意味着「原数组中 k 个 1」刚好可以让数组中长度为 k 的子数组的和为 k
// （使用前缀和可快速得到所有长度为 k 的区域中的数值之和 sum，k - sum 即为移动次数）
// 2. 或者 ：统计窗口中0的个数即为交换的次数
var minChange = function (nums, k) {
    // count表示符合条件的1的个数， index为存放符合条件位置的数组，times为最少交换次数
    let count = 0, index = [], times = Infinity;
    // 遍历nums得到index数组和count
    for(let i = 0; i < nums.length; i++) {
        if (nums[i] < k) {
            count++;
            index[i] = 1;
        }
    }
    // 当全都不符合或者全都符合条件，最少交换次数0
    if (!index.length || count === nums.length) return times;
    // 此处利用滑动窗口思想，拿到当前位置将数据组合在一起所需交换次数changeTime,
    // 将它和times作比较，取最小交换次数
    for(let i = 0; i < index.length - count; i++) {
        // 1. count - sum;
        let changeTime = count - index.slice(i, count).reduce((a, b) => a+b);
        // 或者 ：统计窗口中0的个数即为交换的次数
        // let changeTime = 0;
        // for(let j of index.slice(i, count)) {
        //     !j ? changeTime++ : null;
        // }
        times = Math.min(changeTimes < times);
    }
    return times;
}
console.log(minChange([1,3,1,4,0], 2))


// 如果元素是undefined，那么这个元素是「存在的」，它的值是undefined，而且是Enumerable可遍历到的。
// 同样的说明还出现在every, filter, forEach, some 等方法中，也就是说， map, every, forEach 等遍历方法，是不会对不存在的元素执行回调函数。

// 所以，数组的元素是可能「不存在」的(empty)，即使是索引 index 在 length 范围内。


// 延伸题目

// 1. 2134. 最少交换次数来组合所有的 1 II

// 代码1： 
// 时间复杂度，超时啦
var minSwaps = function(nums) {
    let count = 0, times = Infinity;
    for(let i = 0; i < nums.length; i++) {
        nums[i] ? count++ : null;
    }
    nums.push(...nums.slice(0, count-1));
    for(let i = 0; i < nums.length - count + 1; i++) {
        let changeTimes = nums.slice(i, count + i).filter(i => !i).length;
        times = Math.min(changeTimes < times);
    }
    return times;
};
console.log(minSwaps([1,1,0,0,1]));

// 代码2：
var minSwaps = function(nums) {
    let count = 0, times = Infinity, sum = [], len = nums.length;
    for(let i = 0; i < nums.length; i++) {
        count += nums[i];
    }
    // 环形拼接
    nums.push(...nums.slice(0, count-1));
    // 每一个位置为当前位置之前所有数和
    sum[-1] = 0;
    for(let i = 0; i < nums.length; i++) {
        sum[i] = sum[i-1] + nums[i];
    }
    for(let i = 0; i < len; i++) {
        let changeTimes = count - (sum[i+count-1] - sum[i-1]);
        times = Math.min(times, changeTimes);
    }
    return times;
};

// 1004. 最大连续1的个数 III

// 代码1： 运行超时
var longestOnes = function(nums, k) {
    let sum = 0, index = [], max = 0;
    // 将值为0的元素下标存入数组index
    for(let i = 0; i < nums.length; i++) {
        !nums[i] ? index.push(i+1) : null;
    }
    // 当没有0或者0的个数不大于k时，最大值为数组长度
    if (index.length <= k) return nums.length;
    // 当反转个数为0时，直接计算
    if (!k) {
        return Math.max(...getMax(index, nums));
    }
    for(let j = 0; j < index.length; j++) {
    	// 以k为滑动窗口长度，删除index的元素
        let temp = [...index];
        temp.splice(j, k);
        let result = getMax(temp, nums);
        max = Math.max(max, ...result);
    }
    return max;
};
// 计算连续1的长度
function getMax(temp, nums) {
	// 当最后一位是0时，index加入len，1时加入len+1
    tnums[nums.length-1] ? temp.push(nums.length + 1) : temp.push(nums.length);
    return temp.map((item, index) => {
        return temp[index] - (temp[index-1] || 0) - 1;
    });
}

// 代码2：
var longestOnes = function(nums, k) {
    let left = right = count = max = 0;
    for(let i = 0; i < nums.length; i++) {
        !nums[i] && count++;
        while(count > k) {
            !nums[left] && count--;
            left++;
        }
        max = Math.max(max, right - left + 1);
        right++;
    }
    return max;
};




// 身高从低到高,身高相同体重从轻到重,体重相同维持原来顺序
var highHeavySort = function (n, high, heavy) {
    high = high.split(' ');
    heavy = heavy.split(' ');
    let result = [], youxu = [];
    for(let i = 0; i < n; i++) {
        youxu.push([Number(high[i]), Number(heavy[i]), i+1]);
        for(let j = youxu.length - 1; j > 0 ; j--) {
            youxu[j-1][0] > youxu[j][0] ||
            (youxu[j-1][0] === youxu[j][0] && youxu[j-1][1] > youxu[j][1]) 
                ? [youxu[j-1], youxu[j]] = [youxu[j], youxu[j-1]]
                : null;
        }
    }
    for(let i of youxu) {
        result.push(i[2]);
    }
    return result;
}

console.log(highHeavySort(4, '100 100 120 130', '40 30 60 50'));
// console.log(highHeavySort(3, '90 110 90', '45 60 45'));
// 给定一个数组
// 树的根节点的值储存在下标1,对于储存在下标n的节点，他的左子节点和右子节点分别储存在下标2*n和2*n+1
// 并且我们用-1代表一个节点为空
// 给定一个数组存储的二叉树,试求从根节点到最小的叶子节点的路径,路径由节点的值组成

var findPath = function (arr, start, path, allPath, min) {
    if (arr[start] === undefined || arr[start] === -1) {
        min[0] = Math.min(path[path.length-1], min[0]);
        allPath.push([...path]);
        return;
    }
    path.push(arr[start]);
    findPath(arr, 2*start, path, allPath, min);
    // 减少重复和多余的递归
    (arr[2*start] !== undefined && arr[2*start+1] !== -1) && findPath(arr, 2*start+1, path, allPath, min);
    path.pop();
}

var minYPathTreeArr = function (arr) {
    arr = arr.split(' ').map(i => Number(i));
    arr.unshift(0);
    let allPath = [];
    // path是每次路径，min是最小叶子节点
    let path = [], min = [Infinity];
    findPath(arr, 1, path, allPath, min);
    for(let i of allPath) {
        if (i[i.length-1] === min[0]) return i;
    }
}

console.log(minYPathTreeArr('5 9 8 -1 -1 7 -1 -1 -1 -1 -1 6'))
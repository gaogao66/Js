// 一辆运送快递的货车
// 运送的快递放在大小不等的长方体快递盒中
// 为了能够装载更多的快递同时不能让货车超载
// 需要计算最多能装多少个快递
// 注：快递的体积不受限制
// 快递数最多1000个
// 货车载重最大50000

var morePackage = function (arr, heavy) {
    let count = 0, h = 0;
    let len = arr.length;
    for(let i = 0; i < len; i++) {
        for(let j = 0; j < len - i; j++) {
            if (arr[j] < arr[j+1]) {
                [arr[j], arr[j+1]] = [arr[j+1], arr[j]];
            }
        }
        h += arr[len-i-1];
        if (h <= heavy) {
            count++;
        } else {
            return count;
        }
    }
}

console.log(morePackage([5,10,2,11], 20))
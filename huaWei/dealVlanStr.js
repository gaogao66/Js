// # 题目42 解析vlan
// 20-21,15,18,30,5-10
// 15
// 输出
// 5-10,18,20-21,30
// 并且要求从小到大升序输出

// n^2 +n
var sort = function (result) {
    for(let i = result.length - 2; i > -1; i--) {
        if (result[i][0] > result[i+1][0]) {
            [result[i], result[i+1]] = [result[i+1], result[i]];
        } else {
            break;
        }
    }
    return result;
}
var inRange = function (range, target, find) {
    let s = Number(range[0]);
    let e = Number(range[1] || 0);
    let t = Number(target);
    let result = [];
    if (e !== 0) {
        if (t >= s && t <= e && !find) {
            find = true;
            let a = [s, t-1];
            let b = [t+1, e];
            s < t-1 ? a = [s, t-1] : s === t-1 ? a = [s] : a = null;
            t+1 < e ? b = [t+1, e] : t+1 === e ? b = [e] : b = null;
            a && result.push(a);
            b && result.push(b);
            return [result, find];
        }
        result.push([s, e]);
    } else {
        s !== t || find ? result.push([s]) : find = true;
    }
    return [result, find];
}
var removeUsedVlan = function (str, target) {
    // 在排序中查找目标并移除
    let result = [], temStr = '', find = false;
    for(let i = 0; i <= str.length; i++) {
        if (str[i] === ',' || !str[i]) {
            let range = temStr.split('-');
            let temp = inRange(range, target, find);
            range = temp[0], find = temp[1];
            range.length ? result.push(...range) : null;
            result = sort(result);
            temStr = '';
        } else {
            temStr += str[i];
        }
    }
    let resultStr = '';
    for(let i of result) {
        resultStr += (i[0] + (i[1] ? '-' + i[1] : '') + ',');
    }
    return resultStr.substr(0, resultStr.length - 1);
}

console.log(removeUsedVlan('20-21,15,18,30,5-10', 15))


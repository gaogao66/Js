// 小明从糖果盒中随意抓一把糖果
// 每次小明会取出一半的糖果分给同学们
// 当糖果不能平均分配时
// 小明可以从糖果盒中(假设盒中糖果足够)取出一个或放回一个糖果
// 小明至少需要多少次(取出放回和平均分配均记一次)能将手中糖果分至只剩一颗
var sendCandy = function (num) {
    let count = 0;
    while (num !== 1) {
        num % 2 ? num++ : num /= 2;
        count++;
    }
    return count;
}

console.log(sendCandy(15))

// 一群孩子做游戏，现在请你根据游戏得分来发糖果，要求如下：

// 1. 每个孩子不管得分多少，起码分到一个糖果。
// 2. 任意两个相邻的孩子之间，得分较多的孩子必须拿多一些糖果。(若相同则无此限制)

// 给定一个数组 arr 代表得分数组，请返回最少需要多少糖果。

// 要求: 时间复杂度为 O(n) 空间复杂度为 O(n)

// 解法1：左右各遍历一次
// 把所有孩子的糖果数初始化为 1;

// 先从左往右遍历一遍，如果右边孩子的评分比左边的高，则右边孩子的糖果数更新为左边孩子的 糖果数加 1;

// 再从右往左遍历一遍，如果左边孩子的评分比右边的高，且左边孩子当前的糖果数不大于右边孩子的糖果数，
// 则左边孩子的糖果数更新为右边孩子的糖果数加 1。
function candy(arr) {
    // write code here
    let result = Array(arr.length).fill(1);
    for (let i = 1; i < arr.length; i++) {
        arr[i] > arr[i - 1] && (result[i] = result[i - 1] + 1);
    }
    for (let i = arr.length - 1; i > -1; i--) {
        arr[i] < arr[i - 1] &&
            result[i - 1] <= result[i] &&
            (result[i - 1] = result[i] + 1);
    }
    console.log(result);
    return result.reduce((a, b) => a + b);
}

// 知识点：贪心思想
// 贪心思想属于动态规划思想中的一种，其基本原理是找出整体当中给的每个局部子结构的最优解，并且最终将所有的这些局部最优解结合起来形成整体上的一个最优解。
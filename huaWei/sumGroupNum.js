// 给定两个整数数组
// array1 array2  数组元素按升序排列
//  假设从arr1 arr2中分别取出一个元素，可构成一对元素
//  现在需要取出k对元素，并对取出的所有元素求和
//  计算和的最小值
//  注意：两对元素对应arr1 arr2的下标是相同的
//        视为同一对元素

var sumGroupNum = function (arr1, arr2, k) {
    let len1 = arr1.splice(0, 1);
    let len2 = arr2.splice(0, 1);
    console.log(arr1, arr2)
    // 用arr存储所有可能的组合元素和并排序，前k个元素和就是最小的和；
    let sum = 0, count = 0;
    let arr = [], index = 0;
    for(let i = 0; i < len1; i++) {
        for(let j = 0; j < len2; j++) {
            arr[index] = arr1[i] + arr2[j];
            index++;
        }
    }
    // 冒泡倒叙
    for(let i = 0; i < arr.length; i++) {
        for(let j = 0; j < arr.length - i; j++) {
            if (arr[j] < arr[j+1]) [arr[j], arr[j+1]] = [arr[j+1], arr[j]];
        }
        sum += arr[arr.length - 1 - i];
        count++;
        if (count === k) return sum; 
    }
}

console.log(sumGroupNum([3, 1, 1, 2], [3, 1, 2, 3], 2))
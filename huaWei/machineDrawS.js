// 机器绘图面积
// 机器启动后按照以下规则来进行绘制直线
// 1. (0,0)点开始尝试沿着横线坐标正向绘制直线
//  直到给定的终点E
// 2. 期间可以通过指令在纵坐标轴方向进行偏移
// offsetY为正数表示正向偏移,为负数表示负向偏移
var machineDrawS = function(arr) {
    // N 指令数，E x的终点
    let E = arr[0][1];
    let start = end = height = 0, S = 0;
    for(let i = 1; i < arr.length; i++) {
        end = arr[i][0];
        let temS = (end-start)*height;
        S += temS;
        start = arr[i][0];
        height = Math.abs(height + arr[i][1]);
    }
    S += (E - start)*height;
    return S;
}

console.log(machineDrawS([[4, 10],[1, 1],[2, 1], [3,1],[4, -2]]));
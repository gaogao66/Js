// 输出为网络模型的最小执行时间
var shortTime = function(arr) {
    function fn (arr, index) {
        let time = 0;
        let head = arr[index];
        let count = 0;
        console.log(head);
        while(count < head.length) {
            count++;
            let temp = head[0];
            temp += (head[count] ? fn(arr, head[count]) : 0);
            console.log(temp, time);
            time = Math.max(temp, time);
        }
        return time;
    }
    return fn(arr, 0)
}
console.log(shortTime([[10, 1, 2],[5],[1,3],[2, 4],[5]]))
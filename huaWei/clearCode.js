// 游戏规则：输入一个只包含英文字母的字符串，
// 字符串中的俩个字母如果相邻且相同，就可以消除。
// 在字符串上反复执行消除的动作，
// 直到无法继续消除为止，
// 此时游戏结束。
// 输出最终得到的字符串长度。
// 输入中包含非大小写英文字母是均为异常输入，直接返回0。

var clearWord = function (str) {
    if (!/[a-zA-Z]+/.test(str)) return 0;
    let code = str[0], count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== code) {
            count > 1 ? (str = str.substr(0, i-count) + str.substr(i), str.substr(count), i = i-count-1) : null;
            count = 1, code = str[i];
        } else {
            count++;
        }
    }
    console.log(str);
    return str.length;
}

// 利用栈
var clearWord = function (str) {
    if (!/[a-zA-Z]+/.test(str)) return 0;
    // code存储栈顶元素
    let code = '', stack = [];
    for (let i of str) {
        // 当元素和栈顶元素相同时出栈，不同入栈，更新code为栈顶元素
       code === i ? stack.pop() : stack.push(i);
       code = stack[stack.length-1];
    }
    console.log(stack);
    return stack.length;
}

console.log(clearWord('mMbccbc'))
// 实现一个整数编码方法
// 使得待编码的数字越小
// 编码后所占用的字节数越小
// 编码规则如下
// 1.编码时7位一组，每个字节的低7位用于存储待编码数字的补码
// 2.字节的最高位表示后续是否还有字节，置1表示后面还有更多的字节，
// 置0表示当前字节为最后一个字节
// 3.采用小端序编码，低位和低字节放在低地址上
// 4.编码结果按16进制数的字符格式进行输出，小写字母需要转化为大写字母

// 输入描述
// 输入的为一个字符串表示的非负整数
// 输出描述
// 输出一个字符串表示整数编码的16进制码流

// 采用小端序编码 所以低字节E8输出在前面
// 高字节07输出在后面

var tenToTwo = function (num) {
    let str = '';
    if (num === 0) return '0'; 
    while (num > 0) {
        let code = num % 2;
        str = code + str;
        num = Math.floor(num / 2);
    }
    return str;
}

var twoToSixTeenCode = function (str) {
    let result = ''
    for(let i = 0; i < str.length; ) {
        let j = 3, sum = 0;
        while(j > -1) {
            sum += str[i] * Math.pow(2, j);
            i++;
            j--;
        }
        switch(sum) {
            case 10 : sum = 'A'; break;
            case 11 : sum = 'B'; break;
            case 12 : sum = 'C'; break;
            case 13 : sum = 'D'; break;
            case 14 : sum = 'E'; break;
            case 15 : sum = 'F'; break;
        }
        result += sum;
    }
    return result;
}

var tenToSixTeen = function (str) {
    let num = Number(str);
    // 转化为二进制
    let twoNumStr = tenToTwo(num);
    console.log(twoNumStr);
    let i, temp, result = '';
    for(i = twoNumStr.length - 7; i > -1; i -= 7) {
        temp = twoNumStr.substr(i, 7);
        temp = i === 0 ? '0' + temp : '1' + temp;
        result += twoToSixTeenCode(temp);
    }
    if (i+7 > 0) {
        temp = twoNumStr.substr(0, i+7);
        while(temp.length !== 8) {
            temp = '0' + temp;
        }
        result += twoToSixTeenCode(temp);
    }
    return result;
}

console.log(tenToSixTeen('0'));
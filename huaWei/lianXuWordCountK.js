// 输入AABAAA,2  输出:1
// 同一字母连续出现最多的A 3次
// 第二多2次  但A出现连续3次

var lianXuWordCountK = function (str, num) {
    let hash = {}, code = str[0], start = 0, arr = [];
    for(let i = 0; i <= str.length; i++) {
        if (code !== str[i]) {
            (hash[code] && hash[code] < i - start) || !hash[code] ? 
            hash[code] = i - start : null;
            start = i;
            code = str[i];
        }
    }
    // console.log(hash);
    for(let i in hash) {
        let index = arr.length - 1;
        while(index > -1 && arr[index] < hash[i]) {
            arr[index+1] = arr[index];
            index--;
        }
        arr[index+1] = hash[i];
    }
    // console.log(arr);
    return arr.length >= num ? arr[num-1] : -1;
}

console.log(lianXuWordCountK('AAAAHHHBBCDHHHH', 3))
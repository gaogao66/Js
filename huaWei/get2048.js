// 给一个数组{1,1,1, 1, 1, 1, 1, 1, 1, 2}
// 分别代表着{2,4,8,16,32,64,128,256,512,1024}的个数，
// 设计一个算法，计算至少相加多少次能得到2048这个数字。

const arr = [2,4,8,16,32,64,128,256,512,1024];
function get2048(counts) {
    let hash = [];
    let sum = 0;
    for(let i = 0; i < counts.length; i++) {
        hash[arr[i]] = counts[i];
    }
    let index = arr.length - 1;
    let count = 0;
    while(sum < 2048 && index > -1) {
        hash[arr[index]] ? (sum += arr[index], hash[arr[index]]--, count++) : index--;
    }
    return sum === 2048 ? count : -1;
}

console.log(get2048([1,1,1, 1, 1, 1, 1, 1, 1, 1]))
// 时间复杂度2n^2
function maxlen(a, b, v) {
    let str = '';
    for(let i = 0; i < a.length; i++) {
        let strTem = '';
        // 先获取最长递增子序列在a中
        for(let j = i; j < a.length; j++) {
            a.charCodeAt(j) + 1 === a.charCodeAt(j+1) ? strTem += a[j] : (strTem ? strTem += a[j] : null,j = Infinity);
        }
        if (strTem.length > str.length) {
            let temp = strTem;
            // 在b中查找是否存在strTem
            for(let k = 0; k < b.length; k++) {
                if (strTem === b.substr(k, temp.length)){
                    let len = 0;
                    // 存在后，计算|a[i] - b[i]|之和小于v的字符串
                    for(let j = i; j < i + temp.length + 1; j++) {
                        if (len > v) {
                            strTem = a.substring(i, j-1);
                            break;
                        }
                        len += Math.abs(a.charCodeAt(j) - b.charCodeAt(j));
                    }
                    len && strTem.length > str.length ? str = strTem : null;
                }
            }
        }
    }
    return str.length;
}
console.log(maxlen('xxcdefg', 'cdefghi', 5))
// 字符转ascii码：用charCodeAt();
// ascii码砖字符：用fromCharCode();


// 符合条件的最长字符串长度

// var maxLenStr = function (str) {
//     let reg = /[a|e|i|o|u|A|E|I|O|U]+/g;
//     let arr = str.match(reg);
//     let max = 0
//     // console.log(arr);
//     for(let i of arr) {
//         max < i.length ? max = i.length : null;
//     }
//     return max;
// }

var maxLenStr = function (str) {
    let reg = /[a|e|i|o|u|A|E|I|O|U]/;
    let max = 0,count = 0;
    for(let i = 0; i < str.length; i++) {
        if (reg.test(str[i])) {
            count++;
        } else {
            max = Math.max(max, count);
            count = 0;
        }
    }
    return max;
}

console.log(maxLenStr('asdbuiodevauufgh'))
// 89333444234.998 => 89, 333, 444, 234.998

function getKNum(num) {
    let len = (num + '').length - parseInt(num).toString().length;
    let str = '';
    if (len > 1) {
        len--;
        // 处理小数部分的精度
        str = '.' + (num * Math.pow(10, len)) % Math.pow(10, len);
    }
    num = parseInt(num);
    while (num > 0) {
        str = ',' + num % 1000 + str;
        num = parseInt(num / 1000);
    }
    console.log(str.substring(1));
    return str.substring(1)
}
getKNum(89333444234.998)

// # 处理小数的计算会遇到各种精度问题，
// # 解决方法：将小数*10^n次方转为整数计算
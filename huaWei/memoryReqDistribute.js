// 分配规则如下
// 1.分配的内存要大于等于内存的申请量
// 存在满足需求的内存就必须分配
// 优先分配粒度小的，但内存不能拆分使用
// 2.需要按申请顺序分配
// 先申请的先分配，有可用内存分配则申请结果为true
// 没有可用则返回false
// 注释：不考虑内存释放

// n^2 + 2n
var memoryReqDistribute = function (memory, reqList) {
    let hash = {};
    let result = [];
    memory.replace(/(\d+):(\d)/g, (all, key, val) => {
        hash[key] = Number(val);
    });
    reqList = reqList.split(',');
    for(let i of reqList) {
        let distance = Infinity, key;
        for(let k in hash) {
            let t = Number(i);
            let b = Number(k);
            let dis = Math.abs(t - b);
            b >= t && distance > dis && (distance = dis, key = b);
        }
        key ? (result.push(true),hash[key] > 1 ? hash[key]-- : delete hash[key]) : result.push(false);
    }
    console.log(hash)
    return result;
}

console.log(memoryReqDistribute('64:2,128:1,32:4,1:128',
    '50,36,64,128,127'))
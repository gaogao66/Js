// 停车人的车最近的车辆的距离是最大的返回此时的最大距离
// 849. 到最近的人的最大距离

var maxDistToClosest = function(seats) {
    // 左右边界
    let l = -1 , r = 0;
    let c = 0;
    while(seats[r] !== undefined) {
        if (seats[r] > 0) {
            c = Math.max(l < 0 ? r : Math.floor((r-l)/2), c);
            l = r;
        } 
        r++;
    }
    // r此时越界
    r--;
    // 最后一个为0
    c = !seats[r] ? Math.max(l <= 0 ? r : r-l, c) : c;
    return c;
};
// 公司用一个字符串来标识员工的出勤信息
// absent:    缺勤
// late:      迟到
// leaveearly:早退
// present:   正常上班

// 现需根据员工出勤信息,判断本次是否能获得出勤奖,
// 能获得出勤奖的条件如下：
// 1.缺勤不超过1次
// 2.没有连续的迟到/早退
// 3.任意连续7次考勤 缺勤/迟到/早退 不超过3次

var checkOnWork = function (str) {
    // absentCount：缺勤个数；lCount：迟到早退数；llian：连续迟到早退；m7Count：长度在7内没正常上班的个数
    let arr = str.split(' '), absentCount = 0, lCount = 0, llian = 0, m7Count = 0;
    let lArr = ['late', 'leaveearly'];
    let left = right = 0;
    for(let i = 0; i < arr.length; i++) {
        right = i;
        arr[i] === 'absent' ? absentCount++ : arr[i] === 'present' ? null : 
        lArr.includes(arr[i+1]) ? (lCount += 2, llian = 1, i++) : lCount++;
        if (llian || absentCount > 1) return false;
        m7Count = absentCount + lCount;
        if (right - left + 1 > 7) {
            m7Count = absentCount + lCount;
            arr[left] !== 'present' ? m7Count-- : null;
            left++;
        } 
        if (m7Count > 3) return false;
    }
    return true;
}

console.log(checkOnWork("present"));
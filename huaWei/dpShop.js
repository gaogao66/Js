// 动态规划
// 购物单
void async function () {
    // Write your code here
    // 考虑每个物品时要考虑每种可能出现的情况，1、主件，2、主件+附件1，3、主件+附件2，4、主件+附件1+附件2，不一定每种情况都出现，只有当存在附件时才会出现对应的情况。
    const first = await readline();
    const base = 10;
    let arr = first.split(' ');
    let N = parseInt(arr[0]) / base;
    let M = parseInt(arr[1]);
    let goods = {};
    for (let i = 1; i <= M; i++) {
        let good = await readline();
        let [v, p, q] = good.split(' ').map(Number);;
        if (q) {
            goods[q] = goods[q] || []
            goods[q].push([v / base, v / base * p]);
        } else {
            goods[i] = goods[i] || []
            goods[i].unshift([v / base, v / base * p]);
        }
    }
    const dp = Array(N + 1).fill(0);

    Object.values(goods).forEach(good => {
        let v = [], w = [];
        const [first, ...rest] = good;
        v.push(first[0]);
        w.push(first[1]);
        if (rest[0]) {
            const [f1, f2] = rest;
            v.push(first[0] + f1[0]);
            w.push(first[1] + f1[1]);
            if (f2) {
                v.push(first[0] + f2[0]);
                w.push(first[1] + f2[1]);
                v.push(first[0] + f1[0] + f2[0]);
                w.push(first[1] + f1[1] + f2[1]);
            }
        }
        for (let j = N; j > -1; j--) {
            for (let s = 0; s < w.length; s++) {
                if (j - v[s] >= 0) {
                    dp[j] = Math.max(dp[j], dp[j - v[s]] + w[s])
                }
            }
        }
    })
    console.log(dp[N] * base)
}()
// 一个工厂有m条流水线， 来并行完成n个独立的作业，该工厂设置了一个调度系统
// 在安排作业时，总是优先执行处理时间最短的作业
// 现给定流水线个数m，需要完成的作业数n，每个作业的处理时间分别为 t1,t2...tn
// 请你编程计算处理完所有作业的耗时为多少
// 当n>m时 首先处理时间短的m个作业进入流水线，其他的等待
// 当某个作业完成时，依次从剩余作业中取处理时间最短的

var factoryFlowTime = function (m, n, arr) {
    if (n > m) {
        // 排序应该自己写的
        arr.sort((a, b) => a-b);
        let col = arr.length % m - 1;
        let resultTime = 0;
        while(col < arr.length) {
            resultTime += arr[col];
            col += m;
        }
        return resultTime;
    } else {
        // 找最大
        return Math.max.apply(null, arr);
    }
}

console.log(factoryFlowTime(3, 5, [8, 4, 3, 2, 10]))
// 一次走1或3
// dfs
var goMoutain  = function (n) {
    if (n === 1 || n === 2) return 1;
    if (n === 3) return 2;
    return goMoutain(n-1) + goMoutain(n-3);
}
// 循环遍历
goMoutain = function (n) {
    let f1 = 1, f2 = 1, f3 = 2, f4;
    f4 = n === 1 || n === 2 ? 1 : 2; 
    for(let i = 4; i <= n; i++) {
        f4 = f3 + f1;
        f1 = f2;
        f2 = f3;
        f3 = f4;
    }
    return f4;
}

// 动态规划
goMoutain = function (n) {
    let dp = [];
    return digui(n);
    function digui (n) {
        if (n === 1 || n === 2) {
            // dp[n] === 1;
            return 1;
        } else if (n === 3) {
            // dp[n] === 2;
            return 2;
        } else {
            !dp[n] ? dp[n] = digui(n-1) + digui(n-3) : null;
            return dp[n];
        }
    }
}

console.log(goMoutain(50));
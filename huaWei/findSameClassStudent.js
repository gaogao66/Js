// # 题目40 寻找同班小朋友
// 幼儿园两个班的小朋友排队时混在了一起
// 每个小朋友都知道自己跟前面一个小朋友是不是同班
// 请你帮忙把同班的小朋友找出来
// 小朋友的编号为整数
// 与前面一个小朋友同班用Y表示
// 不同班用N表示

// 输入描述：
// 输入为空格分开的小朋友编号和是否同班标志
// 比如 6/N 2/Y 3/N 4/Y
// 表示一共有4位小朋友
// 2和6是同班 3和2不同班 4和3同班
// 小朋友总数不超过999
//  0< 每个小朋友编号 <999
//  不考虑输入格式错误

//  输出两行
//  每一行记录一班小朋友的编号  编号用空格分开
//  并且
//  1. 编号需要按照大小升序排列，分班记录中第一个编号小的排在第一行
//  2. 如果只有一个班的小朋友 第二行为空
//  3. 如果输入不符合要求输出字符串ERROR
var findSameClassStudent = function (str) {
    let arr = str.split(' ');
    let res1 = [], res2 = [];
    let font = 2;
    for(let i of arr) {
        if (!/^\d+\/[N|Y]$/.test(i)) return 'ERROR'
        i = i.split('/');
        i[1] === 'N' 
        ? font === 1 
        ? (res2.push(i[0]), font = 2) 
        :( res1.push(i[0]), font = 1)
        : font === 1 
        ? (res1.push(i[0]), font = 1)
        : (res2.push(i[0]), font = 2);
    }
    res1.sort((a,b) => Number(a) - Number(b))
    res2.sort((a,b) => Number(a) - Number(b))
    return [res1, res2];
}

console.log(findSameClassStudent('1/N 2/Y 3/N 4/Y'))
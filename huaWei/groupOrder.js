// 题目41 排列组合n阶排列（ok）
var fn = function (arr, path, n, k) {
    if (path.length === n) {
        result.push([...path]);
        return;
    }
    for(let i = 0; i < arr.length; i++) {
        path.push(arr[i]);
        let temp = [...arr];
        temp.splice(i, 1);
        fn(temp, path, n, k);
        path.pop();
        if (result.length === k) {
            return;
        }
    }
}
var groupOrder = function (n, k) {
    let path = [], result = [], arr = Array(n).fill(1).map((i, index) => index+1);
    fn(arr, path, n, k);
    return result[k-1];
}

console.log(groupOrder(2,2));
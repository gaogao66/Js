// 构造一个长度为n的数组，满足以下特点：
// 1.数组中的数最大不超过k；
// 2.数组不重复；
// 3.数组所有数之和等于x。
// 输入n，k，x；
// 以空格为区分，输出数组中的数。
// 如：输入：4 6 15
//        输出：1 3 5 6

var makeArr = function (n, k, x) {
    let arr = Array(k).fill(1).map((i, index) => index+1);
    let result = [], tem = [], sum = 0, start = 0;
    fn(arr, tem, sum, start, x, n);
    return result;
    function fn(arr, tem, sum, start, target, len) {
        if (tem.length === len) {
            sum === target ? result.push([...tem]) : null;
            return;
        }
        for(let i = start; i < arr.length; i++) {
            tem.push(arr[i]);
            sum += arr[i];
            // 利用start记录arr开始下标保证不向前找
            fn(arr, tem, sum, i+1, target, len);
            let p = tem.pop();
            sum -= p;
        }
    }
}

console.log(makeArr(4, 6, 15))
// 定义女：0， 男： 1
// 分类讨论
// 水平方向
var row = function (arr) {
    let max = 0;
    for(let i = 0; i < arr.length; i++) {
        let front, count = 0;
        for(let j = 0; j < arr[i].length; j++) {
            if (arr[i][j]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    return max;
}
// 竖直方向
var column = function (arr) {
    let max = 0;
    for(let i = 0; i < arr[0].length; i++) {
        let front, count = 0;
        for(let j = 0; j < arr.length; j++) {
            if (arr[j][i]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    return max;
}
// 对角线
var xie = function (arr) {
    let max = 0;
    // 下三角
    for(let l = 0; l < arr.length ; l++) {
        let front, count = 0;
        for(let i = l, j = 0; i < arr.length && j < arr[0].length; j++, i++) {
            if (arr[i][j]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    // 上三角
    for(let l = 1; l < arr[0].length ; l++) {
        let front, count = 0;
        for(let i = 0, j = l; i < arr.length && j < arr[0].length; j++, i++) {
            if (arr[i][j]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    return max;
}
var fanXie = function (arr) {
    let max = 0;
    // 上三角
    for(let l = 0; l < arr[0].length ; l++) {
        let front, count = 0;
        for(let i = 0, j = l; i < arr.length && j > -1; j--, i++) {
            if (arr[i][j]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    // 下三角
    for(let l = 1; l < arr.length ; l++) {
        let front, count = 0;
        for(let i = l, j = arr[0].length - 1; i < arr.length && j > -1; j--, i++) {
            if (arr[i][j]) {
                count = front ? count + 1 : 1;
                front = 1;
            } else {
                front = 0;
            }
            max = Math.max(max, count);
        }
    }
    return max;
}
var getLianCount = function (arr) {
    let rowMax = row(arr);
    let colMax = column(arr);
    let xieMax = xie(arr);
    let fanXieMax = fanXie(arr);
    return Math.max(rowMax, colMax, xieMax, fanXieMax);
}

console.log(getLianCount([
    [1,0,0,1],
    [1,1,0,1],
    [1,1,1,0]
]))
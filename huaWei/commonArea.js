// 题目描述

// 给出3组点坐标(x, y, w, h)，-1000<x,y<1000，w,h为正整数。
// (x, y, w, h)表示平面直角坐标系中的一个矩形：
// x, y为矩形左上角坐标点，w, h，向右w，向下h。
// (x, y, w, h)表示 x 轴 (x, x+w) 和 y 轴 (y, y-h) 围成的矩形区域
// (0, 0, 2, 2)表示 x 轴 (0, 2) 和 y 轴 (0, -2) 围成的矩形区域
// (3, 5, 4, 6)表示 x 轴 (3, 7) 和 y 轴 (5, -1) 围成的矩形区域
// 求3组坐标构成的矩形区域重合部分的面积。

// 解决思路：找到3个矩形面积有重叠的时候的特点，左上角坐标(xMax, ymin), 右下角坐标(xlenMin, ylenMax)
// 当w,h > 0 时，说明有重叠否则没有
var commonArea = function(arr) {
    let xMax = 0, ymin =Infinity, xlenMin = Infinity, ylenMax = 0;
    let w,h;
    for(let i of arr) {
        xMax = Math.max(xMax, i[0]);
        ymin = Math.min(ymin, i[1]);
        xlenMin = Math.min(xlenMin, i[0]+i[2]);
        ylenMax = Math.max(ylenMax, i[1]-i[3]);
    }
    w = xlenMin - xMax;
    h = ymin - ylenMax;
    console.log(w, h, xMax, ymin, xlenMin, ylenMax);
    return w>0 && h>0 ? w*h : 0;
}

console.log(commonArea([[0, 3, 1, 1], [3, 5, 1, 1], [2, 3, 2, 2]]))

// 
// 223.请你计算并返回两个矩形覆盖的总面积。
// 每个矩形由其 左下 顶点和 右上 顶点坐标表示：
var computeArea = function(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) {
    let repeatArea = 0;
    let x1Max = Math.max(ax1, bx1),
        y1Max = Math.max(ay1, by1),
        x2Min = Math.min(ax2, bx2),
        y2Min = Math.min(ay2, by2);
    if (x2Min-x1Max > 0 && y2Min-y1Max > 0) {
        repeatArea = (x2Min-x1Max)*(y2Min-y1Max);
    }
    return (ax2-ax1)*(ay2-ay1) + (bx2-bx1)*(by2-by1) - repeatArea;
};

// 困难： 计算平面中所有 rectangles 所覆盖的 总面积 。任何被两个或多个矩形覆盖的区域应只计算 一次 。
// 超时
var rectangleArea = function(rectangles) {
    // 离散化
    let s = 0, high = [];
    for(let i of rectangles) {
        let count = i[0];
        while(count < i[2]) {
            count++;
            high[count] = Math.max(high[count] || 0, i[3]-i[1]);
        }
    }
    return high.reduce((a, b) => (a+b)%(10^9+7));
};

// 解决方法：线性扫描+离散化+线段树




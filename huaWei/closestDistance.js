// 题目35 最近距离
// 同一个数轴x有两个点的集合A={A1,A2,...,Am}和B={B1,B2,...,Bm}
// A(i)和B(j)均为正整数
// A、B已经按照从小到大排好序，AB均不为空
// 给定一个距离R 正整数，列出同时满足如下条件的
// (A(i),B(j))数对
// 1. A(i)<=B(j)
// 2. A(i),B(j)之间距离小于等于R
// 3. 在满足1，2的情况下每个A(i)只需输出距离最近的B(j)
// 4. 输出结果按A(i)从小到大排序

var closestDistance = function (aLen, bLen, R, str1, str2) {
    let a = str1.split(' ').map(i => Number(i));
    let b = str2.split(' ').map(i => Number(i));
    let result = [];
    for(let i = 0; i < aLen; i++) {
        let distance = Infinity, bj;
        for(let j = 0; j < bLen; j++) {
            let temDis = Math.abs(a[i] - b[j])
            a[i] <= b[j] && temDis <= R && distance > temDis && (distance = temDis, bj = b[j]);
        }
        bj ? result.push([a[i], bj]) : null;
    }
    return result;
}

console.log(closestDistance(4, 5, 5,
    '1 5 5 10',
    '1 3 8 8 20'))
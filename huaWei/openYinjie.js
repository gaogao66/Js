// 相对开音节构成的结构为辅音+元音(aeiou)+辅音(r除外)+e
// 常见的单词有bike cake
// 给定一个字符串，以空格为分隔符, 反转每个单词的字母, 若单词中包含如数字等其他非字母时不进行反转
// 反转后计算其中含有相对开音节结构的子串个数
// (连续子串中部分字符可以重复)------这条件说明不能直接使用正则匹配来实现

// # match匹配到符合条件的字符串后，在/g下会接着匹配剩余的字符，而不会匹配匹配过的

var reverseStr = function (str) {
    // 字符串无法通过下标交换来反转（应该是因为字符串是基本类型）
    let newStr = ''
    for(let i = str.length - 1; i > -1; i--) {
        newStr += str[i];
    }
    return newStr;
}

var structWord = function (str, reg) {
    let count = 0;
    for(let i = 0; i <= str.length - 4; i++) {
        count += str.substr(i, 4).match(reg)?.length || 0
    }
    return count;
}

var openYinjie = function (str) {
    let reg = /[^a-z]+/g;
    let structWordReg = /[^a|e|i|o|u][a|e|i|o|u][^a|e|i|o|u|r]e/g;
    let arr = str.split(' ');
    let count = 0;
    for(let i = 0; i < arr.length; i++) {
        if (!reg.test(arr[i])) arr[i] = reverseStr(arr[i]);
        // 连续子串中部分字符可以重复,所以不能直接使用正则匹配
        count += structWord(arr[i], structWordReg);
    }
    return count;
}

console.log(openYinjie('!ekam a ekekac'));
// 题目31 统计整数由连续自然数相加的数量统计

// 依然利用了滑动窗口的思路
var equalNumLXSum = function (num) {
    let count = 0;
    let index = num;
    let left = right = 1;
    let sum = 0;
    let result = [], path = [];
    while(index > 0) {
        path.push(right);
        sum += right;
        while(sum > num) {
            path.shift();
            sum -= left;
            left++;
        }
        sum === num ? (count++, result.unshift(`${num}=${[...path].join('+')}`)) : null;
        index--;
        right++;
    }
    result.push(`Result:${count}`)
    return result;
}

console.log(equalNumLXSum(9));
// 给出一个整型数组 numbers 和一个目标值 target，请在数组中找出两个加起来等于目标值的数的下标，返回的下标按升序排列。
// （注：返回的数组下标从1开始算起，保证target一定可以由数组里面2个数字相加得到）
function twoSum(numbers, target) {
    const map = new Map();
    let result = [];
    for (let i = 0; i < numbers.length; i++) {
        const cha = target - numbers[i];
        if (map.has(cha)) {
            result.push([map.get(cha) + 1, i + 1]);
        }
        map.set(numbers[i], i);
    }
    return result.length > 1 ? result : result[0] || [];
}

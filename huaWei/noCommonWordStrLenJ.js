// 题目48 两个没有相同字符的元素长度乘积的最大
var noCommonWordStrLenJ = function (arr) {
    let result = 0;
    for(let i = 0; i < arr.length; i++) {
        for(let j = i+1; j < arr.length; j++) {
            // 此处判断利用set结构减少了对比字符串字母的过程
            let temp = arr[i] + arr[j];
            let set = new Set(temp);
            if (temp.length === set.size) {
                result = Math.max(result, arr[i].length * arr[j].length);
            }
        }
    }
    return result;
}

console.log(noCommonWordStrLenJ(['iwdvpbn','hk','iuop','iikd','kadgpf']))
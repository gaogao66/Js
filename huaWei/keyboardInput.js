function getNumInput(str) {
    return str.replace(/\//, '');
}

function dictory(code, count) {
    let str = '';
    let three = count % 3;
    three === 0 ? three = 3 : null;
    let four = count % 4;
    four === 0 ? four = 4 : null;
    three--; four--;
    switch(code) {
        case '0': str = ' ';break;
        case '1': str = count % 2 ? ',' : '.';break;
        case '2': str = ['a', 'b', 'c'][three];break;
        case '3': str = ['d', 'e', 'f'][three];break;
        case '4': str = ['g', 'h', 'i'][three];break;
        case '5': str = ['j', 'k', 'l'][three];break;
        case '6': str = ['m', 'n', 'o'][three];break;
        case '7': str = ['p', 'q', 'r', 's'][four];break;
        case '8': str = ['t', 'u', 'v'][three];break;
        case '9': str = ['w', 'x', 'y', 'z'][four];break;
    }
    return str;
}

function getEngInput(str) {
    let result = ''
    let fcode = str[0], count = 0;
    for(let i = 0; i < str.length; i++) {
        if (fcode !== str[i] || str[i] === '/') {
            console.log(fcode, count);
            result += dictory(fcode, count);
            str[i] === '/' ? (fcode = str[i+1], count = -1) : (fcode = str[i], count = 0);
        }
        count++;
    }
    console.log(fcode, count);
    result += dictory(fcode, count);
    return result;
}

function keyboardInput (str) {
    let arr = str.split('#');
    let result = ''
    let lang = 'num';
    for(let i of arr) {
        result += lang === 'num' ? (lang = 'eng', getNumInput(i)) : (lang = 'num', getEngInput(i));
    }
    return result;
}

console.log(keyboardInput('#222233'))
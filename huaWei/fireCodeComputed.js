// 已知火星人使用的运算符号为#;$
// 其与地球人的等价公式如下
// x#y=2*x+3*y+4
// x$y=3*x+y+2
// x y是无符号整数
// 地球人公式按照c语言规则进行计算
// 火星人公式中$符优先级高于#相同的运算符按从左到右的顺序运算

var fireCode = function (str) {
    // x#y=2*x+3*y+4
    // x$y=3*x+y+2
    let num = '',stack = [], frontNum = 0, is$ = false;
    // 处理$部分
    for(let i = 0; i < str.length; i++) {
        if (!Number.isNaN(Number(str[i]))) {
            num = Number(num + str[i]);
        } else {
            is$ ? (frontNum = stack.pop(), stack.push(3*frontNum + num + 2)) : stack.push(num);
            num = '';
            str[i] === '$' ? is$ = true : (is$ = false, stack.push(str[i]));
        }
    }
    stack.push(num);
    console.log(stack)
    let a = stack[0] , b = stack[0+2];
    // 处理#部分
    for(let i = 2; i < stack.length; i += 2) {
        a = 2*a + 3*b + 4;
        b = stack[i+2];
    }
    return a;
}

console.log(fireCode('7#6$5#12'))
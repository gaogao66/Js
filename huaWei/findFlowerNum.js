// 所谓的水仙花数是指一个n位的正整数其各位数字的n次方的和等于该数本身，
// 返回长度是N的第M个水仙花数，
// 个数从0开始编号，
// 若M大于水仙花数的个数返回最后一个水仙花数和M的乘积，
// 若输入不合法返回-1 --输入不合法不知如何判断（9，1）
var IsFlower = function (num, n) {
    let tNum = num, sum = 0;
    while(tNum > 0) {
        sum += Math.pow(tNum % 10, n);
        tNum = parseInt(tNum / 10);
    }
    if (sum === num) return true;
    return false;
}
var FindflowerNum = function (n, m) {
    // 位数为n的水仙花数在start-end的范围内
    let start = Math.pow(10,n-1);
    let end = Math.pow(10, n);
    let count = 0, lastChild = 0;
    for(let i = start; i < end; i++) {
        // 判断是否是水仙花数
        if (IsFlower(i, n)) {
            lastChild = i;
            count++;
            if (count - 1 === m) {
                return lastChild;
            }
        }
    }
    return m > count ? lastChild*m : lastChild;
}

console.log(FindflowerNum(3, 0))
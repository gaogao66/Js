// 解压密码
// 	3[k]2[mn]--kkkmnmn 3[m2[c]]--mccmccmcc

// 思路： 通过用例发现需要考虑嵌套和不嵌套的情况；
// 1. 通过正则匹配字符串中‘3[k]’这中形式的字符串；
// 2. 判断匹配是否有结果；
// 3. 匹配有结果则取遍历匹配结果arr;
// 4. 每一个子项通过获取到重复次数和重复的字符子串，计算重复的字符串结果并替换到str中；
// 5. 重复1——4部，最后返回str

var jieya = function (str) {
    let arr = str.match(/\d+\[[a-z]*\]/g);
    while(arr) {
        for(let i of arr) {
            let result = '';
            let c = i.match(/\d+/g);
            let code = i.match(/(?<=\[)([a-z]*)(?=\])/g);
            console.log(i, c, code)
            while(c) {
                c--;
                result += code;
            }
            console.log("result", result)
            str = str.replace(i,result);
        }
        console.log(str);
        arr = str.match(/\d+\[[a-z]*\]/g);
    }
    return str;
}

console.log(jieya('3[m2[c4[d]]]'))

// 解压字符串
// 减少利用正则的部分，完全通过判断又影响性能吧
var extractStr = function (str) {
    let num = '', result = '';
    for(let i of str) {
        if (Number.isNaN(Number(i))) {
            if (/[a-z]/.test(i)) {
                if (num) {
                    let tempLen = num.length;
                    num = Number(num);
                    let numLen = num.toString().length;
                    // num是否符合要求
                    if (num > 2 && tempLen === numLen) {
                        while(num) {
                            result += i;
                            num--;
                        }
                        num = '';
                    } else {
                        return '!error';
                    }
                } else {
                    result += i;
                }
            } else {
                return '!error';
            }
        } else {
            num += i;
        }
    }
    return result;
}

console.log(extractStr('4dbbf3c'));
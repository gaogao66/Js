// Gpu算力 同 factoryFlowTime
// 需要尽可能多的将任务交给GPU执行，
// 现在有一个任务数组，
// 数组元素表示在这1s内新增的任务个数，
// 且每秒都有新增任务，
// 假设GPU最多一次执行n个任务，
// 一次执行耗时1s，
// 在保证Gpu不空闲的情况下，最少需要多长时间执行完成。

// 思路： 如果当下1s新增任务arr[i]小于等于n,则时间+1，否则则将当下的时间计算，并将剩余任务加入下一个1s任务中
var gpuTime = function (n, len, arr) {
    let count = 0, rest = 0;
    for(let i = 0; i < len; i++) {
        arr[i] += rest;
        rest = 0;
        arr[i] <= n ? count++ : (rest = arr[i] % n,count += Math.floor(arr[i] / n));
    }
    rest ? count++ : null;
    return count;
}

console.log(gpuTime(4, 5, [5,4,1,1,11]))

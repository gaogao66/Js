// 输出源字符串中的第K小的ascii码值字母的下标
var codeIndexK = function (str, k) {
    let newStr = str.split(''), word, len = newStr.length;
    // 冒泡找到那个字母
    for(let i = 0; i < len; i++) {
        for(let j = 1; j < len-i; j++) {
            let f = newStr[j-1].charCodeAt();
            let b = newStr[j].charCodeAt();
            if (f > b) {
                [newStr[j-1], newStr[j]] = [newStr[j], newStr[j-1]];
            }
        }
        if (len < k) {
            word = newStr[len-1];
            break;
        }
        if (i === len - k) {
            word = newStr[k - 1];
            break;
        }
    }
    return str.indexOf(word);
}

console.log(codeIndexK('AbCdeFG', 3));
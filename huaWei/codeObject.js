// @前是全集合，之后是已占用的集合，输出剩余的未战用集合（以原本的顺序）
var codeObject = function(str) {
    let arr = str.split('@');
    let all = arr[0].split(','), used = arr[1].split(',');
    let alls = fn(all), useds = fn(used);
    let result = '';
    console.log(alls, useds)
    // 因为key都是字母，而非整数，否则不可以通过for in 去遍历
    for(let i in alls) {
        used[i] ? alls[i] -= useds[i] : null;
        alls[i] ? result += `,${i}:${alls[i]}` : null;
    }
    return result.slice(1);
    function fn (arr) {
        let obj = {};
        for(let i = 0; i < arr.length; i++) {
            const [key, val] = arr[i].split(':');
            obj[key] = Number(val);
        }
        return obj;
    }
}

console.log(codeObject('a:3,b:5,c:2@a:1,b:2'))
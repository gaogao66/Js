// 题目30 求矩阵的最大值
// 给定一个仅包含0和1的n*n二维矩阵,请计算二维矩阵的最大值,计算规则如下
// 1、每行元素按下标顺序组成一个二进制数(下标越大约排在低位)，二进制数的值就是该行的值，矩阵各行之和为矩阵的值
// 2、允许通过向左或向右整体循环移动每个元素来改变元素在行中的位置
// 比如
// [1,0,1,1,1]   向右整体循环移动两位  [1,1,1,0,1]
// 二进制数为11101 值为29
// [1,0,1,1,1]   向左整体循环移动两位  [1,1,1,1,0]
// 二进制数为11110 值为30
var twoToTen = function (arr) {
    let sum = 0;
    let count = 0;
    for(let i = arr.length - 1; i > -1; i--) {
        sum += arr[i] * Math.pow(2, count);
        count++;
    }
    return sum;
}

var getTwoMoveMax = function (arr) {
    let maxVal = 0;
    let count = 0;
    let len = arr.length;
    // 让它整体完整移动，每一次计算当前值是否是最大的
    while(count < len) {
        maxVal = Math.max(maxVal, twoToTen(arr));
        count++;
        arr.unshift(...arr.splice(len-1, 1));
    }
    return maxVal;
}

var juzhenMaxVal = function (n, arr) {
    let sum = 0;
    for(let i of arr) {
        sum += getTwoMoveMax(i);
    }
    return sum;
}

console.log(juzhenMaxVal(5, [[1,0,0,0,1],
[0,0,0,1,1],
[0,1,0,1,0],
[1,0,0,1,1],
[1,0,1,0,1]]))

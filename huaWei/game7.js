// 喊7 是一个传统的聚会游戏
// N个人围成一圈，按顺时针从1-7编号
// 编号为1的人从1开始喊数
// 下一个人喊得数字是上一个人喊得数字+1
// 但是当将要喊出数字7的倍数或者含有7的话
// 不能喊出 而是要喊过

// 假定N个人都没有失误。当喊道数字k时,可以统计每个人喊 “过"的次数
// 现给定一个长度n的数组,存储打乱的每个人喊”过"的次数,请把它还原成正确顺序
// 即数组的第i个元素存储编号i的人喊“过“的次数

var createArr7 = function (n) {
    let result = [7];
    let count = 1, num = 7, i = 2;
    while(count < n) {
        let beiNum = num * i;
        let has7 = num + 10 * (i - 1);
        result.push(beiNum, has7);
        count += 2;
    }
    return result;
}

var game7 = function (arr) {
    let len = arr.length;
    let guoCount = arr.reduce((a, b) => a+b);
    // 更据数组算出喊过的个数即7的个数，计算出k即上限，然后再计算属于哪一个元素
    let arr7 = createArr7(guoCount);
    let index;
    let result = Array(len).fill(0);
    console.log(arr7);
    for(let i of arr7) {
        index = i % len - 1;
        result[index]++;
    }
    return result;
}

console.log(game7([0, 0, 0, 2, 1]))
// easy
// 找到好朋友的位置，好朋友即是离你最近的比你大的下标位置
var findFriendPosition = function (height) {
    let friendPosition = [];
    for(let i = 0; i < height.length; i++) {
        let len = friendPosition.length;
        for(let j = i + 1; j < height.length; j++) {
            if (height[i] < height[j]) {
                friendPosition.push(j);
                break;
            }
        }
        if (len === friendPosition.length) friendPosition.push(0);
    }
    return friendPosition;
}

console.log(findFriendPosition([123, 124, 125, 121, 119, 122, 126, 123]))
// nginx反向代理接口跨域
// 跨域原理：同源策略是浏览器的安全策略，不是http协议的一部分。
// 服务器端调用http接口只是使用http协议，不执行js脚本，不需要同源策略，不存在跨域问题。

// 实现：通过nginx配置一个代理服务器，反向代理访问domain2接口，并可以顺便修改cookie中的domain信息，方便当前域cookie写入，实现跨域登录。

// nginx具体配置
// #proxy服务器
// server {
//     listen       81;
//     server_name  www.domain1.com;

//     location / {
//         proxy_pass   http://www.domain2.com:8080;  #反向代理
//         proxy_cookie_domain www.domain2.com www.domain1.com; #修改cookie里域名
//         index  index.html index.htm;

//         # 当用webpack-dev-server等中间件代理接口访问nignx时，此时无浏览器参与，故没有同源限制，下面的跨域配置可不启用
//         add_header Access-Control-Allow-Origin http://www.domain1.com;  #当前端只跨域不带cookie时，可为*
//         add_header Access-Control-Allow-Credentials true;
//     }
// }

// 前端
// var xhr = new XMLHttpRequest();
// xhr.withCredentials = true; // 前端开关：浏览器是否读写cookie
// xhr.open('get', 'http://www.domain1.com:81/?user=admin', true);
// xhr.send();

// Nodejs后台
// var http = require("http");
// var server = http.createServer();
// var qs = require("querystring");
// server.on("request", (req, res) => {
//   let params = qs.parse(req.url.substring(2));
//   res.writeHead(200, {
//     'set-cookie': 'l=a123456;Path=/;Domain=www.domain2.com;HttpOnly'
//   })
//   res.write(JSON.stringify(params));
//   res.end();
// });
// server.listen('8080');



// Nodejs中间件代理跨域
// node中间件实现跨域代理，原理大致同nginx,都是通过启动一个代理服务器，实现数据的转发，也可以通过设置cookieDomainRewrite参数
// 修改响应头中cookie中域名，实现当前域的cookie写入，方便接口登录认证

// 1. 非vue(2次跨域)
// 前端——同上面前端 url: http://www.domain1.com:3000/login?user=admin

// 中间件服务器
// var express = require("express");
// var proxy = require("http-proxy-middleware");
// var app = express();

// app.use("/", proxy({
//   target: 'http://www.domain2.com:8080',
//   changeOrigin: true,
//   // 修改响应头信息，实现跨域并允许携带cookie
//   onProxyRes: function(proxyRes, req, res) {
//     res.header("Access-Control-Allow-Origin", "http://www.domain1.com");
//     res.header("Access-Control-Allow-Credentials", true);
//   },
//   // 修改响应信息中的cookie域名
//   cookieDomainRewrite: 'www.domain1.com' 
// }))

// app.listen(3000);

// Nodejs后台同上

// 2. vue(1次跨域)：利用node+webpack+webpack-dev-server代理接口跨域。在开发环境下，
// 由于vue渲染服务和接口代理服务都是同一个webpack-dev-server，所以页面与代理接口之间不再跨域，无须设置headers跨域信息了。

// module.exports = {
//   entry: {},
//   module: {},
//   ...
//   devServer: {
//       historyApiFallback: true,
//       proxy: [{
//           context: '/login',
//           target: 'http://www.domain2.com:8080',  // 代理跨域目标接口
//           changeOrigin: true,
//           secure: false,  // 当代理某些https服务报错时用
//           cookieDomainRewrite: 'www.domain1.com'  // 可以为false，表示不修改
//       }],
//       noInfo: true
//   }
// }

// websocket

// 前端
{/* <script src="https://cdn.bootcss.com/socket.io/2.2.0/socket.io.js"></script>
<script>
var socket = io('http://www.domain2.com:8080');

// 连接成功处理
socket.on('connect', function() {
    // 监听服务端消息
    socket.on('message', function(msg) {
        console.log('data from server: ---> ' + msg); 
    });

    // 监听服务端关闭
    socket.on('disconnect', function() { 
        console.log('Server socket has closed.'); 
    });
});

document.getElementsByTagName('input')[0].onblur = function() {
    socket.send(this.value);
};
</script> */}

// Nodejs socket后台
// var http = require("http");
// var socket = require("socket.io");

// // 启动http服务
// var server = http.createServer((req, res) => {
//   res.writeHead(200, {
//     "Content-type": "text/html"
//   });
//   res.end();
// });

// server.listen(8080);

// //监听socket连接
// socket.listen(server).on("connection", client => {
//   // 接收信息
//   client.on("message", msg => {
//     client.send("hello"+msg);
//   });
//   client.on("disconnect", () => {
//   // 断开处理
//   });
// })
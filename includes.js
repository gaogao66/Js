// 字符串包含某个字符串

// indexOf()

// search(): 返回值-1，或者首次出现的下标index
// 检索字符串中指定的子字符串，或与正则表达式相匹配的字符串

var str = "12331232";
console.log(str.search('3'));

// match()：返回匹配到的元素数组
// 找到一个或多个正则表达式的匹配
console.log(str.match(/3/g));

// test: 返回true, false, 
// 检索字符串中指定的值是否存在reg.test(str)
console.log(/3/.test(str));

// exec(): 返回一个数组，匹配到的结果, null
// 检索字符串中的正则表达式的匹配
console.log(/3/.exec(str));